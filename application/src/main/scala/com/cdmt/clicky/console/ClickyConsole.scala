/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.console

import com.cdmt.clicky.bidder.Bidder
import com.cdmt.clicky.domain.publisher.OpportunityCoordinates

/**
  * Created by Unger
  *
*/

object ClickyConsole extends App {
  println("Enter input...")

  var continue = true
  while(continue) {
    Console.readLine("Enter command: ") match {

      case "exit" => {
        println("Exiting")
        continue = false
      }

      case "bid" => {
        val bid = Bidder().adsFor(OpportunityCoordinates.example)
        println(bid)
      }

      case command: String => {
        println("Executing command")
      }

      case _ => {
        println("Command not found")
      }
    }
  }
}