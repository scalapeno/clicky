/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.adserver

import com.cdmt.clicky.bidder.Bidder
import com.cdmt.clicky.clickredirect.ClickRedirect
import com.cdmt.clicky.domain.advertiser.{CreativeId, CreativeInstance, ImpressionId}
import com.cdmt.clicky.domain.events.ClickParam
import com.cdmt.clicky.domain.publisher.OpportunityCoordinates
import com.cdmt.clicky.services.ZipUrlService
import com.cdmt.clicky.utilities.z._
import com.cdmt.clicky.utilities.{Problem, Url}

/**
  *
  * Created by Unger  *
  **/

//Todo: Can config web stuff here too
object AdServer {

  val prodInstance = new AdServer()

  def apply() = prodInstance
}

class AdServer() {

  def opportunityFor(oppCoords: OpportunityCoordinates): Problem Or RedirectAd = {
    val creative = Bidder().adsFor(oppCoords) //AdCreative //todo: Locate from Coords

    //todo: If we're attaching bids here, doesn't that make this a bidder as opposed to an ad server?
    //perhaps the bidder is the part that intelligently adjusts the bid.
    // The server packages the bid with the creative
    val impId = ImpressionId.create()
//    val creative = AdCreative

    val bid = CPCAdBid(10000)
    val ad = NativeAdPackage.example
    //Todo tracking params onto displayUrl =-> tracking Url
    val clParam = ZipUrlService().zipToClickParam(ad.displayUrl, impId, oppCoords.oppId, oppCoords.partnerId, oppCoords.ppid, CreativeId.example, CreativeInstance.example.creativeInstanceId)
    val clickUrl = ClickRedirect().redirectUrl(clParam)

    val redir = RedirectAd(ad, clickUrl)
    redir.succ
  }

}

case class RedirectAd(ad: AdPackage, clickUrl: Url)

object RedirectAd {

//  implicit val writes = Json.writes[RedirectAd]

  val exampleClickUrl = ClickRedirect().redirectUrl(ClickParam.example)
  val example = RedirectAd(AdPackage.example, exampleClickUrl)
}