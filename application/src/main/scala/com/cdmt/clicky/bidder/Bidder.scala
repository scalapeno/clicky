/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.bidder

import com.cdmt.clicky.adserver.AdCreative
import com.cdmt.clicky.domain.publisher.OpportunityCoordinates

/**
  *
  * Created by Unger  *
  **/

object Bidder {
  val prodInstance = new Bidder
  def apply() = prodInstance
}

class Bidder {

  def adsFor(oppCoords: OpportunityCoordinates): List[AdCreative] = List(AdCreative.example)

}
