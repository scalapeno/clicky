package com.cdmt.clicky.clicklogger

import com.cdmt.clicky.domain.ValueClassesForDomain.ZippedString
import com.cdmt.clicky.domain.events.{ClickFieldsNotZippable, ClickFieldsZippable}
import com.cdmt.clicky.services.{ClickPersistanceService, ZipUrlService}
import com.cdmt.clicky.utilities.FutureUtils.FireFuture
import com.cdmt.clicky.utilities.{Problem, Url}
import com.cdmt.clicky.utilities.ValueClassesForUtilities.Millis
import scalaz.{NonEmptyList, \/}

class ClickLogger {
  def logClick(millis: Millis, userId: String, payload: String, ua: String, ip: String): NonEmptyList[Problem] \/ Url = {
    val zipPayload = ZippedString(payload)
    val unzippableFieldsO = ClickFieldsNotZippable(millis, ip, ua, userId)
    val zippedFieldsO = ZipUrlService().unpackToClickFields(zipPayload)

    //todo: This doesn't handle errors properly. [Note: why not?]
    val clickEventO = for {
      zf <- zippedFieldsO.leftMap(p => NonEmptyList.apply(p))
      uz <- unzippableFieldsO
    } yield ClickFieldsZippable.buildClickEvent(zf, uz)

    clickEventO.map {
      case (clickEvent, url) => {

        FireFuture {
          //Side effecting!
          //todo: How do we deal with this side effecting behavior?
          //It's a producer. Hide this behind some Akka infrastructure.
          ClickPersistanceService().persist(clickEvent)
        }
        url
      }
    }
  }
}

object ClickLogger {
  val prod = new ClickLogger()

  def apply(): ClickLogger = prod
}