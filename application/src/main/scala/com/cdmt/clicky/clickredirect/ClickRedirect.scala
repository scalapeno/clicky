/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.clickredirect

/**
  *
  * Created by Unger  *
  **/

import com.cdmt.clicky.domain.events.ClickParam
import com.cdmt.clicky.utilities.{Logging, Url}

object ClickRedirect {
  val prodInstance = new ClickRedirect
  def apply() = prodInstance

  val clickParamName = "cl"
  val redirectEndpoint = "cl"
}

class ClickRedirect extends Logging {

  def redirectUrl(clickParam: ClickParam): Url = {
    val url = s"http://localhost:8080/${ClickRedirect.redirectEndpoint}?${ClickRedirect.clickParamName}=" + clickParam.param
    Url(url).getOrElse {
      warn(s"Could not construct redirect url: $clickParam")
      throw new IllegalStateException()
    }
  }
}

/*
Click redirect features:
Store datapoints independent of opp/imp:
  UserID, Device, Location
Time stored in timezone for user, advertiser, and publisher independently
Country, City, *DMA, *LatLon based Location
DeviceType, OS, captured
 */
