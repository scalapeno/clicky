package com.cdmt.clicky.services

import com.cdmt.clicky.domain.RuleSetId
import com.cdmt.clicky.utilities.z._
import com.cdmt.clicky.utilities.{Problem, Template}
import org.apache.commons.io.IOUtils

/**
  * Created by randy on 4/7/16.
  */

object TemplateService {
  val prodInstance = new TemplateService
  def apply() = prodInstance
}

class TemplateService {

  val cl = Thread.currentThread().getContextClassLoader

  def templateForRuleSetId(rsid: RuleSetId): Problem Or Template = {
    val template = IOUtils.toString(cl.getResourceAsStream("iframeTemplate.js"))

//    val fetchTemplate = Template("""window.clicky = {};
//                                   |window.clicky.live = true;
//                                   |
//                                   |var div = document.createElement('div');
//                                   |div.setAttribute("id", "clickyDiv");
//                                   |div.innerHTML = '<iframe src="http://localhost:8080/t/t/@@" ' +
//                                   |    'width="300px" height="100px" ' +
//                                   |    'marginwidth="0" marginheight="0" ' +
//                                   |    'hspace="0" vspace="0" frameborder="0" scrolling="no">' +
//                                   |    '</iframe>';
//                                   |
//                                   |var pos = document.getElementById('clickyPos');
//                                   |pos.parentNode.insertBefore(div, pos);""".stripMargin)

    Template(template).succ
  }

}
