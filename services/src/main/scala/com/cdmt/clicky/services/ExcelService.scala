/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.services

/**
  *
  * Created by Unger
  *
  **/


import java.io.File

import org.apache.poi.xssf.usermodel._

import scala.collection.JavaConversions._

class ExcelService extends ExcelReadService with ExcelWriteService {

//  def readRowToObject[T: CanBeLine](file: File) = {
//    val wb = new XSSFWorkbook(file)
//    val sheet = wb.getSheetAt(0)
//    sheet.rowIterator().foreach(row => {
////      CanBeLine.fromLine()
//    })
//  }

}

trait ExcelReadService {

  def readToCampaign(file: File) = {
    val wb = new XSSFWorkbook(file)
    val sheet = wb.getSheetAt(0)
    sheet.foreach(row => {
      val org = row.getCell(0)
      val adv = row.getCell(1)
      val camp = row.getCell(2)
      val tacticGroup = row.getCell(3)
      val tactic = row.getCell(4)
      val creative = row.getCell(5)
      val title = row.getCell(6)
      val landingPage = row.getCell(7)
    })
  }


}

trait ExcelWriteService {

}