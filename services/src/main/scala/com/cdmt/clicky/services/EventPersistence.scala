/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.services

import java.io._

import akka.actor.ActorSystem
import com.amazonaws.services.s3.AmazonS3Client
import com.cdmt.clicky.domain.events.{ClickEvent, OpportunityEvent}
import com.cdmt.clicky.infrastructure.aws.{EventLogType, S3Config}
import com.cdmt.clicky.utilities._
import org.apache.commons.io.FileUtils

import scala.collection._
import scala.concurrent.duration._

/**
  * Created by Unger
  **/


object EventPersistence {

  val oppPersistence = new S3LogPersistence[OpportunityEvent](EventLogType.opportunityEvent)
  val clickPersistence = new S3LogPersistence[ClickEvent](EventLogType.clickEvent)

}

//todo: This whole thing doesn't seem thread safe
//Need to lock buffer, copy to new buffer, wipe old buffer. Then unlock old and proceed w/ new buffer.
class S3LogPersistence[T: CanBeLine](eventType: EventLogType[T]) extends Logging { //extends ClickPersistence

  val writeFrequency = 1.minute
  val clickLogFile = new File("/tmp/clicky/", eventType.label)  //todo: Change file name as time progresses

  import scala.concurrent.ExecutionContext.Implicits.global
  implicit val akkaSystem = ActorSystem("LogPersistenceSystem")

  val bufferedLogs = mutable.Buffer[Line]() //todo: Put actors in front of buffer to prevent need for locking

  akkaSystem.scheduler.scheduleOnce(writeFrequency)(archive())

  def persist(event: T): Unit = {

    info("serializing line")
    val line = serialize(event) //Why serialize before buffer instead of after?

    if(FeatureToggles().writeEventsToS3) {
      info(s"Buffering line: ${line.str}")
      buffer(line)
    }

    if(FeatureToggles().writeEventsToDisk){
      info(s"Logging line to ${clickLogFile.getAbsolutePath}")
      val appendTrue = true
      FileUtils.writeStringToFile(clickLogFile, line.withNewline, "UTF-8", appendTrue)
    }

  }

  def serialize(event: T): Line = {
    val line = Line(event)
    line
  }

  //Buffering in memory exposes logs to loss if crashes. Problem?
  def buffer(logLine: Line): Unit = {
    bufferedLogs.append(logLine)

  }

  val s3 = new AmazonS3Client()
  def archive(): Unit = {

    val bucketName = eventType.rawLogBucketName

    if(bufferedLogs.isEmpty) {
      info(s"No logs to write to bucket $bucketName, skipping")
      akkaSystem.scheduler.scheduleOnce(writeFrequency)(archive())
    } else {
      val is = lineBufferIntoInputStream(bufferedLogs)

      val keyName = S3Config().makeKeyName()

      info(s"Writing to bucket: $bucketName")
      try {
        s3.putObject(bucketName, S3Config().makeKeyName(), is, null) //todo Null?!
        bufferedLogs.clear()
      } catch {
        case ex: Exception => {
          error("BAD! Could not write to bucket.", ex)
          //todo: write to disk instead.
        }
      } finally {
        akkaSystem.scheduler.scheduleOnce(writeFrequency)(archive())
      }
    }
  }

  //Todo: move to utils
  def lineBufferIntoInputStream(lineCollection: mutable.Buffer[Line]): InputStream = {
    //todo: more generic collection type
    LineFile(lineCollection).toInputStream
  }

}

//Todo: Make generic in log type via typeclass
class DiskLogPersistence() extends Logging {

  val writePath = "/tmp/clicky/clickLogs"
  val clickLogFile = new File(writePath, "clickLogs")  //todo: Change file name as time progresses

  def serialize(click: ClickEvent): Line = {
    val line = implicitly[CanBeLine[ClickEvent]].toLine(click)
    line
  }

  def persist(click: ClickEvent) = {
    val line = serialize(click)
    //todo: buffer lines in memory before writing to disk
    info(s"Logging line to ${clickLogFile.getAbsolutePath}")
    val appendTrue = true
    FileUtils.writeStringToFile(clickLogFile, line.withNewline, "UTF-8", appendTrue)
  }

  def readBackStr(): String = {
    val clicksStr = FileUtils.readFileToString(clickLogFile)
    clicksStr
  }
}

