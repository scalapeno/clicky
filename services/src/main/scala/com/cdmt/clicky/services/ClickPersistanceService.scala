/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.services

/**
  *
  * Created by Unger
  *
  **/


import com.cdmt.clicky.domain.events.ClickEvent
import com.cdmt.clicky.infrastructure.aws.RedShift
import com.cdmt.clicky.utilities.Url
import com.cdmt.clicky.utilities.z._

object ClickPersistanceService {

  private val prodInstance = new RedShiftClickPersistance()

  def apply() = prodInstance
}

trait ClickPersistanceService {

  def persist(ce: ClickEvent): Unit = {
    //Write to Kinesis?
//    import scala.concurrent.ExecutionContext.Implicits.global
//    Future()
    Url.example.succ
  }

}

class RedShiftClickPersistance extends ClickPersistanceService {
  //Write to Redshift directly for now

  override def persist(ce: ClickEvent): Unit = {
    RedShift().persist(ce)
  }


}