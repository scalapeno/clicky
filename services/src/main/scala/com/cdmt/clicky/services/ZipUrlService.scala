/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.services

/**
  *
  * Created by Unger
  *
  **/


import java.util.Base64
import java.util.zip.{Deflater, Inflater}

import com.cdmt.clicky.domain.PPID
import com.cdmt.clicky.domain.ValueClassesForDomain._
import com.cdmt.clicky.domain.advertiser._
import com.cdmt.clicky.domain.events.{ClickFieldsZippable, ClickParam}
import com.cdmt.clicky.utilities.ValueClassesForUtilities._
import com.cdmt.clicky.utilities._
import com.cdmt.clicky.utilities.z._

object ZipUrlService {
  private val prodInstance = new ZipUrlService
  def apply() = prodInstance

  val b64Enc = Base64.getUrlEncoder
  val b64Dec = Base64.getUrlDecoder
}

//Output must be valid URL chars
class ZipUrlService {

  def unpackToClickFields(payload: ZippedString): Problem Or ClickFieldsZippable = {
    val line = Line(unzip64(payload)).head
    val o = CanBeLine.fromLine[ClickFieldsZippable](line)
    o
  }

  def zipToClickParam(landingPageUrl: Url, impressionId: ImpressionId, oppId: OppId, partnerId: PartnerId, pPID: PPID, creativeId: CreativeId, instHash: HashId): ClickParam = {
    val clickFieldsZippable = ClickFieldsZippable(landingPageUrl, impressionId, oppId, partnerId, pPID, creativeId, instHash)
    val paramLine = CanBeLine.toLine(clickFieldsZippable)
    val zipped = zip64(paramLine.str)
    ClickParam(zipped.raw)
  }

  def zip64(s: String) = {
    val inB = s.getBytes(StringUtilities.UTF8)
    val out = compress(inB)
    val b64 = ZipUrlService.b64Enc.encode(out)
    val str = new String(b64)
    ZippedString(str)
  }

  def unzip64(s: ZippedString) = {
    val bytes64 = s.raw.getBytes()
    val bytes = ZipUrlService.b64Dec.decode(bytes64)
    val out = decompress(bytes)
    val str = new String(out)
    str
  }

  def compress(inData: Array[Byte]): Array[Byte] = {
    val deflater = new Deflater(Deflater.BEST_SPEED, true)
    deflater.setInput(inData)
    deflater.finish()
    val compressedData = new Array[Byte](inData.size * 2)
    val count: Int = deflater.deflate(compressedData)
    compressedData.take(count)
  }

  def decompress(inData: Array[Byte]): Array[Byte] = {
    val inflater = new Inflater(true)
    inflater.setInput(inData)
    val decompressedData = new Array[Byte](inData.size * 2)
    var count = inflater.inflate(decompressedData)
    var finalData = decompressedData.take(count)
    while (count > 0) {
      count = inflater.inflate(decompressedData)
      finalData = finalData ++ decompressedData.take(count)
    }
    finalData
  }

}
