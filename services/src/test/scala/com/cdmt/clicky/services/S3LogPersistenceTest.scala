/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.services

/**
  *
  * Created by Unger
  *
  **/


import com.cdmt.clicky.domain.events.ClickEvent
import org.scalatest.FunSuite

class LogPersistenceTest extends FunSuite {

  test("Serialize to disk"){
    val cp = new DiskLogPersistence()
    val click = ClickEvent.example
    val serializedClick = cp.serialize(click)
    println(click)
    assert(serializedClick.str == "EECC33^98793875938745^127.0.0.1^iPad^00000000000000000000000000000000^123~456-AB_CDE")
  }

}

object ClickPersistenceToDiskTest extends App {

  val cp = new DiskLogPersistence()
  val click = ClickEvent.example
  cp.persist(click)
  cp.persist(click)

  val str = cp.readBackStr()
  println(str)
}

//object S3PersistenceTest extends App {
//  val s3 = new S3LogPersistence()
//  s3.archive()
//}