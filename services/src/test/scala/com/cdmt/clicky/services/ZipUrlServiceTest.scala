/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.services

/**
  *
  * Created by Unger
  *
  **/


import org.scalatest.FunSuite

class ZipUrlServiceTest extends FunSuite {

  test("testZip - back and forth") {
    val in = "A test string Here it is What if we make the string a lot longer Will we start to see an advantage from zipping"
    println(s"Orig: $in")
    println(in.size)
    val b64 = new String(ZipUrlService.b64Enc.encode(in.getBytes()))
    println(s"B64: $b64")
    println(b64.size)
    val out = ZipUrlService().zip64(in)
    println(s"Zipped: $out")
    println(out.raw.size)

    val unzipped = ZipUrlService().unzip64(out)
    println(s"Unzipped: $unzipped")

    assert(in == unzipped)
  }

  test("testZip - byte arrays") {
    val in = "A test string! Here it is."
    println(s"Orig: $in")
    val inBytes = in.getBytes("Utf-8")
    println(s"Orig bytes: ${inBytes.mkString(",")}")

    val out = ZipUrlService().compress(inBytes)
    println(s"Zipped: ${new String(out, "Utf-8")}")

//    val forDec = (new String(out, "utf-8")).getBytes("utf-8")
    val forDec = out

    val dec = ZipUrlService().decompress(forDec)
    val unzipped = new String(dec)
    println(s"OutBytes: ${dec.mkString(",")}")
    println(s"Unzipped: $unzipped")

    assert(in == unzipped)
  }

}
