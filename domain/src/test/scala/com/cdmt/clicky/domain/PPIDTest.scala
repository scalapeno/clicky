/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.domain

/**
  *
  * Created by Unger
  *
  **/


import org.scalatest.FunSuite

class PPIDTest extends FunSuite {

  test("PPID back and forth"){
    val ex = PPID.example.toString
    println(s"ex: $ex")
    val res = PPID(ex)
    println(s"res: $res")
    assert(res.isRight)
  }


}
