/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.domain

/**
  *
  * Created by Unger
  *
  **/


import org.scalatest.FunSuite

class IpAddressTest extends FunSuite {

  test("valid") {
    val addr = "192.168.0.1"
    val ip = IpAddress(addr)
    assert(ip.toOption.isDefined)
  }

  test("invalid") {
    val addr = "HI.168.0.1"
    val ip = IpAddress(addr)
    assert(ip.toOption.isEmpty)
  }
}
