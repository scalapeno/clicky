/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.domain.advertiser

/**
  *
  * Created by Unger
  *
  **/


import org.scalatest.FunSuite

class OppIdTest extends FunSuite {

  test("example works"){

    val ex1 = OppId.example
    val ex2 = OppId.example

    assert(ex1 == ex2)

  }

}
