/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.adserver

/**
  *
  * Created by Unger  *
  **/


import com.cdmt.clicky.domain.advertiser._
import com.cdmt.clicky.utilities.ValueClassesForUtilities.{HashId, MicroDollars, Title}
import com.cdmt.clicky.utilities.{Hash, Url}
import play.api.libs.json.{JsNumber, JsValue, Writes}

case class AdCreative(instance: CreativeInstance, creative: Creative, tactic: Tactic, tacticGroup: TacticGroup, campaign: Campaign)

object AdCreative {
  //todo: Does this even need to exist?
  val example = AdCreative(CreativeInstance.example, Creative.example, Tactic.example, TacticGroup.example, Campaign.example)
}

trait AdBid {
  def granularity: Int
}

object AdBid {
  implicit val writes = new Writes[AdBid] {
    override def writes(adBid: AdBid): JsValue = adBid match {
      case cpc: CPCAdBid => JsNumber(cpc.cpc.raw)
      case cpm: CPMAdBid => JsNumber(cpm.cpm.raw)
    }
  }
  val example: AdBid = CPCAdBid(10000)
}

case class CPCAdBid(cpc: MicroDollars) extends AdBid {
  val granularity = 1
}
case class CPMAdBid(cpm: MicroDollars) extends AdBid {
  val granularity = 1000
}

case class NativeAdPackage(bid: AdBid, creativeId: CreativeId, image: Url, title: Title, displayUrl: Url) extends AdPackage {
  override def creativeInstanceHash: HashId = Hash().string(title, image.toString)
}

object NativeAdPackage {

  val example = NativeAdPackage(
    AdBid.example
    , CreativeId.example
    , Url("http://images.taboola.com/taboola/image/fetch/f_jpg%2Cq_80%2Ch_135%2Cw_202%2Cc_fill%2Cg_faces%2Ce_sharpen/http%3A/cdn.taboolasyndication.com/libtrc/static/thumbnails/0b9fd3757591ed82065c760c4ab1a547.jpg").getOrElse(???)
    ,"Why You Should Be Getting Your Wine Online"
    ,Url("http://www.huffingtonpost.com/2013/04/30/wine-club-lot18_n_3165408.html").getOrElse(???)
  )

//  implicit val adWrites = Json.writes[NativeAdPackage]

}


trait AdPackage {
  def creativeId: CreativeId
  def creativeInstanceHash: HashId
  def bid: AdBid
}

object AdPackage {

//  implicit val adWrites = new Writes[AdPackage] {
//    override def writes(o: AdPackage): JsValue = o match {
//      case n: NativeAdPackage => Json.toJson(n)
//    }
//  }

  val example = NativeAdPackage.example

}