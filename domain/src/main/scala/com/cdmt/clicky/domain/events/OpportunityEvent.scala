/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.domain.events

import com.cdmt.clicky.domain.ValueClassesForDomain._
import com.cdmt.clicky.domain._
import com.cdmt.clicky.domain.advertiser.{OppId, PartnerId}
import com.cdmt.clicky.utilities._
import com.cdmt.clicky.utilities.z._

import scalaz.syntax.apply._

/**
  *
  * Created by Unger
  *
  **/

//todo: Should this fully express RTB model?
case class OpportunityEvent(
                             oppId: OppId
                             , partnerId: PartnerId
                             , url: Url
                             , userId: Guid
                             , ipAddress: IpAddress
                             , userAgent: UserAgentHeader
                             , ppid: PPID
                             , ruleSetId: RuleSetId
                           )

object OpportunityEvent {

  def apply(opp: String, part: String, url: String, user: String, ip: String, ua: String, ppid: String, rule: String): NonEmptyList[Problem] Or OpportunityEvent = {
    val x = (OppId(opp).validation.toValidationNel |@|
      PartnerId(part).validation.toValidationNel
      ){(oppId: OppId, partnerId: PartnerId) => {

      ???.asInstanceOf[OpportunityEvent]
    }}
    x.disjunction
  }

  val example = OpportunityEvent(OppId.example, PartnerId.example, Url.example
    , Guid.example, IpAddress.example, UserAgentHeader("test UA"), PPID.example, RuleSetId.example
  )

  implicit val opportunityEventCanBeLine = new CanBeLine[OpportunityEvent] {
    override def toLine(opp: OpportunityEvent): Line = {
      val components = List(
        CanBeStringSerDes.ser(opp.oppId)
        ,CanBeStringSerDes.ser(opp.partnerId)
        ,CanBeStringSerDes.ser(opp.url)
        ,CanBeStringSerDes.ser(opp.userId)
        ,CanBeStringSerDes.ser(opp.ipAddress)
        ,CanBeStringSerDes.ser(opp.userAgent)
        ,CanBeStringSerDes.ser(opp.ppid)
        ,CanBeStringSerDes.ser(opp.ruleSetId)
      )
      val joined = components.mkString("^")
      Line(joined).headOption.getOrElse(Line.empty) //todo: HeadOption or else Empty seems strange...
    }

    override def fromLine(line: Line): Problem Or OpportunityEvent = {
      val tokens = line.tokens
       val oppO = OpportunityEvent(
        tokens(0)
        ,tokens(1)
        ,tokens(2)
        ,tokens(3)
        ,tokens(4)
        ,tokens(5)
        ,tokens(6)
        ,tokens(7)
      )
      oppO.leftMap(n => Problem(n))
    }
  }

}