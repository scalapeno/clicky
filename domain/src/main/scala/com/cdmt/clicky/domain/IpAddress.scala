/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.domain

/**
  * Created by Unger
*/

import com.cdmt.clicky.utilities.z._
import com.cdmt.clicky.utilities.{CanBeStringSerDes, Problem, ValidationProblem}
import org.apache.commons.validator.routines.InetAddressValidator

class IpAddress private(val str: String) {

  def isLocalhost = IpAddress.isLocalhost(this)

  override def toString = str
  override def equals(that: Any) = that match {
    case thatIp: IpAddress => this.str == thatIp.str
    case _ => false
  }
  override def hashCode = str.hashCode
}

object IpAddress {
  def apply(str: String): Problem Or IpAddress= {
    if (InetAddressValidator.getInstance().isValid(str))
      Succ(new IpAddress(str))
    else ValidationProblem(str, "IpAddress").fail
  }

  implicit val ipCanBeStringSerDes = new CanBeStringSerDes[IpAddress] {
    override def ser(t: IpAddress): String = t.toString
    override def des(str: String): Option[IpAddress] = IpAddress(str).toOption
  }

  val localhostv4 = new IpAddress("127.0.0.1")
  val localhostv6 = new IpAddress("0:0:0:0:0:0:0:1")

  def isLocalhost(ipAddress: IpAddress) = {
    ipAddress == localhostv4 || ipAddress == localhostv6
  }

  val example = new IpAddress("104.173.242.239")
}