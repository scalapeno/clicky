/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.domain

/**
  *
  * Created by Unger  *
  **/


import com.cdmt.clicky.utilities.CanBeStringSerDes
import com.cdmt.clicky.utilities.ValueClassesForUtilities._


case class DeviceType(typ: DeviceType.Type)

case class OS(name: Name)

case class DeviceInfo(os: OS, deviceType: DeviceType)

object OS {

  val example = OS("Windows")

  implicit val osCBSD = CanBeStringSerDes.mkCBSSD[OS](_.name.raw, str => Some(OS(str)))

}

object DeviceType {

  def apply(label: Label): DeviceType = {
    val typ = labelToType.getOrElse(label, UNK)
    DeviceType(typ)
  }

  implicit val deviceTypeCBSD = CanBeStringSerDes.mkCBSSD[DeviceType](_.typ.label.raw, str => Some(DeviceType(str)))

  trait Type {
    def label: Label
  }

  def mkType(l: Label): Type = new Type {
    val label = l
  }

  val Mobile = mkType("Mobile")
  val Desktop = mkType("Desktop")
  val Tablet = mkType("Tablet")
  val Console = mkType("Console")
  val DMR = mkType("DMR")
  val Wearable = mkType("Wearable")
  val UNK = mkType("UNKNOWN")

  val types = List(Mobile, Desktop, Tablet, Console, DMR, Wearable, UNK)
  val labelToType = types.map(d => d.label -> d).toMap

  val example = DeviceType(Mobile)

}