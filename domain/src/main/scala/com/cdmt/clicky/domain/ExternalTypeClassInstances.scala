/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.domain

import com.cdmt.clicky.utilities.CanBeStringSerDes
import org.joda.time.DateTime

/**
  *
  * Created by Unger
  **/

object ExternalTypeClassInstances {

  implicit val dateTimeCanBeStringSerDes = new CanBeStringSerDes[DateTime] {
    override def ser(t: DateTime): String = t.toString // Todo: Don't need millis, right?
    override def des(str: String): Option[DateTime] = Option(DateTime.parse(str))
  }

}
