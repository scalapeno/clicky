/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.domain.publisher

/**
  *
  * Created by Unger
  *
  **/


import com.cdmt.clicky.domain.ValueClassesForDomain.UserAgentHeader
import com.cdmt.clicky.domain.advertiser.{OppId, PartnerId, PubRuleSetId, UserId}
import com.cdmt.clicky.domain.{IpAddress, PPID}
import com.cdmt.clicky.utilities.z.{NonEmptyList => _, _}
import com.cdmt.clicky.utilities.{Problem, Url}

import scalaz.Scalaz._
import scalaz._

//Todo: Does time belong here? Or is that a meta item?
case class OpportunityCoordinates(
                                   oppId: OppId
                                   , partnerId: PartnerId
                                   , pubRuleSetId: PubRuleSetId
                                   , url: Url
                                   , ppid: PPID
                                   , user: UserId
                                   , ip: IpAddress
                                   , ua: UserAgentHeader
                                 )

object OpportunityCoordinates {

  //Perform validation on string inputs
  def apply(partnerId: String, pubRuleSetId: String, pageUrl: String
            , ppid: String, user: String, ipAddr: String, userAgent: String): Or[NonEmptyList[Problem], OpportunityCoordinates] = {

    val oppCoordsValidatorParams =
      PartnerId(partnerId).validationNel |@|
      PubRuleSetId(pubRuleSetId).validationNel |@|
      Url(pageUrl).validationNel |@|
      PPID(ppid).validationNel |@|
      UserId(user).validationNel |@|
      IpAddress(ipAddr).validationNel //|@|
//      UserAgent(userAgent).validationNel

    val oppCoordsV = oppCoordsValidatorParams {
      case (prId, prsId, url, pp, u, ip) => {
        val id = OppId.create()
        val ua = UserAgentHeader(userAgent)
        OpportunityCoordinates(id, prId, prsId, url, pp, u, ip, ua)
      }
    }

    oppCoordsV.disjunction
  }

  val example = OpportunityCoordinates(
    OppId.example
    ,PartnerId.example
    ,PubRuleSetId.example
    ,Url.example
    ,PPID.example
    ,UserId.example
    ,IpAddress.example
    ,UserAgentHeader("ExampleUA")
  )
}