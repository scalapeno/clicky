/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.domain

/**
  *
  * Created by Unger
  *
  **/


import java.util.regex.Pattern

import com.cdmt.clicky.utilities.ValueClassesForUtilities._
import com.cdmt.clicky.utilities._
import com.cdmt.clicky.utilities.z._

class PPID private(ppidStr: String) extends ValidatedString[PPID] {
  protected val raw = ppidStr
}

object PPID extends ValidatedStringCompanion[PPID] {

  val ppidRegex = Pattern.compile("^[~\\-_\\w]*$")

  def validate(s: String) = {
    if(ppidRegex.matcher(s).matches() && s.length <= 32) {
      new PPID(s).succ
    }
    else ValidationProblem(s, "PPID").fail
  }

  val example = new PPID("123~456-AB_CDE")
}

class RuleSetId(rsidStr: String) extends ValidatedString[RuleSetId] {
  protected val raw = rsidStr
}

object RuleSetId extends ValidatedStringCompanion[RuleSetId]{
  val example = new RuleSetId("ABCD123")

  val rsIdRegex = Pattern.compile("^[~\\-_\\w]*$")

  override def validate(str: String): Or[Problem, RuleSetId] = {
    if(rsIdRegex.matcher(str).matches() && str.length <= 32)
      new RuleSetId(str).succ
    else ValidationProblem(str, "RuleSetId").fail

  }

  implicit val ruleSetIdCanBeStringSerDes = CanBeStringSerDes.mkCBSSD[RuleSetId](_.raw, str => RuleSetId(str).toOption)
}
