/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.domain.events

import com.cdmt.clicky.domain.advertiser.{CreativeId, ImpressionId, OppId}
import com.cdmt.clicky.utilities.ValueClassesForUtilities._
import org.joda.time.DateTime

/**
  *
  * Created by Unger
  *
  **/

case class ImpressionServedEvent(
                                  impId:ImpressionId
                                  , oppId: OppId
                                  , timestamp: DateTime
                                  , creativeIds: List[CreativeId]
                                  , creativeInstanceIds: List[HashId]
                                )

object ImpressionServedEvent {
  val example = ImpressionServedEvent(
    ImpressionId.example
    , OppId.example
    , new DateTime(1452986318000l)
    , List(CreativeId.example)
    , List(HashId(938475092834982034l))
  )
}