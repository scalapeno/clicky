/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.domain

import com.cdmt.clicky.domain.ValueClassesForDomain._
import com.cdmt.clicky.utilities.CanBeStringSerDes

/**
  * Created by Unger
  */

object ValueClassesForDomain extends TypeClassesForValueClassesForDomain {

  case class UserAgentHeader(raw: String) extends AnyVal

  case class ZippedString(raw: String) extends AnyVal

}

trait TypeClassesForValueClassesForDomain {

  implicit val uahCanBeSerDes = new CanBeStringSerDes[UserAgentHeader] {
    override def ser(t: UserAgentHeader): String = t.raw
    override def des(str: String): Option[UserAgentHeader] = Option(UserAgentHeader(str))
  }

}
