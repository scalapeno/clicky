/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.domain.events

import com.cdmt.clicky.domain.ValueClassesForDomain._
import com.cdmt.clicky.domain._
import com.cdmt.clicky.domain.advertiser._
import com.cdmt.clicky.utilities.StringUtilities._
import com.cdmt.clicky.utilities.ValueClassesForUtilities._
import com.cdmt.clicky.utilities._
import com.cdmt.clicky.utilities.z._

import scalaz.syntax.apply._
/**
  * Created by Unger
  */

/*
Clicks are unique by rcvd time. Only one click per Impression per Millisecond.
ClickEvent has three representations:
1) Input received, comes from a serialized representation. (ClickFields)
2) On-server representation. Deserialized, fully hydrated. (ClickEvent)
3) Output format(s?). Going to a serialized representation. (ClickRow)
 */
case class ClickEvent(
                       imp: ImpressionId
                       , opp: OppId
                       , partnerId: PartnerId
                       , ppid: PPID
                       , rcvd: Millis
                       , userId: UserId
                       , ip: IpAddress
                       , ua: UserAgentHeader
                     )

object ClickEvent {

  def apply(imp: String, opp: String, part: String, ppid: String, rcvd: Millis, userId: String,
            ip: String, ua: String): NonEmptyList[Problem] Or ClickEvent = {
    ??? //todo: Implement
  }



  val example = ClickEvent(
    ImpressionId.example
    , OppId.example
    , PartnerId.example
    , PPID.example
    , Millis(98793875938745l)
    , UserId.example
    , IpAddress.example
    , UserAgentHeader("iPad")
  )

  implicit val clickEventCanBeLine = new CanBeLine[ClickEvent] {
    override def toLine(t: ClickEvent): Line = {
      val components = List(
        implicitly[CanBeStringSerDes[ImpressionId]].ser(t.imp)
        ,implicitly[CanBeStringSerDes[Millis]].ser(t.rcvd)
        ,implicitly[CanBeStringSerDes[IpAddress]].ser(t.ip)
        ,implicitly[CanBeStringSerDes[UserAgentHeader]].ser(t.ua)
        ,implicitly[CanBeStringSerDes[UserId]].ser(t.userId)
        ,implicitly[CanBeStringSerDes[PPID]].ser(t.ppid)
      )
      val joined = components.mkString("^")
      Line(joined).headOption.getOrElse(Line.empty) //todo: HeadOption or else Empty seems strange...
    }

    override def fromLine(line: Line): Or[Problem, ClickEvent] = {
      val tokens = line.tokens
      val cvO = ClickEvent(
        tokens(0)
        ,tokens(1)
        ,tokens(2)
        ,tokens(3)
        ,tokens(4).tryToLong.getOrElse(???)
        ,tokens(5)
        ,tokens(6)
        ,tokens(7)
      )
      cvO.leftMap(n => Problem(n))
    }
  }
}

case class ClickParam(param: String)

object ClickParam {
  val example = ClickParam("NY1BCoQwEAS_0l_Zmz_wPGAbh41JmGkU9vUbDx6KuhTUB2IKqfBWsDAIFzyxHja94yZO-xI6-FaG2jVphYHVa32ilIWgjiRhDbZd1mSF2KOf-PkY8_AH")
}

case class ClickFieldsZippable(landingPageUrl: Url, impId: ImpressionId, oppId: OppId, partnerId: PartnerId, ppid: PPID, creativeId: CreativeId, creativeInstanceHash: HashId)

case class ClickFieldsNotZippable(rcvd: Millis, ip: IpAddress, ua: UserAgentHeader, userId: UserId)

object ClickFieldsNotZippable {
  def apply(rcvd: Millis, ip: String, ua: String, userId: String): NonEmptyList[Problem] Or ClickFieldsNotZippable = {
    val params = IpAddress(ip).validationNel |@|
      UserId(userId).validationNel

    val cfnz = params {
      case (i, u) => ClickFieldsNotZippable(rcvd, i, UserAgentHeader(ua), u)
    }

    cfnz.disjunction
  }
}

object ClickFieldsZippable {

  def apply(url: String, impId: String, oppId: String, partnerId: String, ppid: String, creativeId: String, ih: String): NonEmptyList[Problem] Or ClickFieldsZippable = {
    val params = Url(url).validationNel |@|
      ImpressionId(impId).validationNel |@|
      OppId(oppId).validationNel |@|
      PartnerId(partnerId).validationNel |@|
      PPID(ppid).validationNel |@|
      CreativeId(creativeId).validationNel |@|
      HashId(ih).validationNel

    val v = params {
      case(u, i, o, p, pp, c, h) => ClickFieldsZippable(u, i, o, p, pp, c, h)
    }
    v.disjunction
  }

  implicit val cfCanBeLine = new CanBeLine[ClickFieldsZippable] {
    override def toLine(cfz: ClickFieldsZippable): Line = {
      val components = List(
        CanBeStringSerDes.ser(cfz.landingPageUrl)
        ,CanBeStringSerDes.ser(cfz.impId)
        ,CanBeStringSerDes.ser(cfz.oppId)
        ,CanBeStringSerDes.ser(cfz.partnerId)
        ,CanBeStringSerDes.ser(cfz.ppid)
        ,CanBeStringSerDes.ser(cfz.creativeId)
        ,CanBeStringSerDes.ser(cfz.creativeInstanceHash)
      )
      val joined = components.mkString("^")
      Line(joined).headOption.getOrElse(Line.empty) //todo: HeadOption or else Empty seems strange...
    }

    override def fromLine(line: Line): Problem Or ClickFieldsZippable = {
      val tokens = line.tokens

      if(line.tokens.length >= 6 ){
        val cfz = ClickFieldsZippable(
          tokens(0)
          ,tokens(1)
          ,tokens(2)
          ,tokens(3)
          ,tokens(4)
          ,tokens(5)
          ,tokens(6)
        )
        cfz.leftMap(n => Problem(n))
      } else {
        Problem("Not enough fields in clickfields").fail
      }
    }
  }

//todo: This ClickEvent should be the largest, including all targeting/logging fields including device/os, country/city, pubHour/advHour, etc.
  def buildClickEvent(zipped: ClickFieldsZippable, unzipped: ClickFieldsNotZippable): (ClickEvent, Url) = { //ValidationNel[FailureWrapper, ClickEvent]
    val ce = ClickEvent(zipped.impId, zipped.oppId, zipped.partnerId, zipped.ppid, unzipped.rcvd, unzipped.userId, unzipped.ip, unzipped.ua)
    val url = zipped.landingPageUrl
    (ce, url)
  }

}

