/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.domain.advertiser

import com.cdmt.clicky.utilities.ValueClassesForUtilities._
import com.cdmt.clicky.utilities.{Hash, Url}

import scala.collection.Set

/**
  *
  * Created by Unger
  *
  **/

//Organization names must be globally unique
case class Organization(id: OrganizationId, name: Name, advertisers: Set[Partner])

object Organization {
  val example = Organization(OrganizationId.example, "Example Organization", Set(Partner.example))
}

//Advertiser names must be globally unique
case class Partner(id: PartnerId, name: Name, campaigns: Set[Campaign], organizationId: OrganizationId) {

}

object Partner {
  val example = Partner(PartnerId.example, "ExamplePartner.co", Set(Campaign.example), OrganizationId.example)
}
/*
  Organization and Partner should span adv/pub. Only one instance of Partner is needed
  in the case where a site is both an advertiser and a publisher.
  The below are adv specific: Campaign, TacticGroup, Tactic, Creative, CreativeInstance.
 */


//Campaigns must be unique within an advertiser
case class Campaign(id: CampaignId, name: Name, tacticGroups: Set[TacticGroup], advertiserId: PartnerId) {

}

object Campaign {
  val example = Campaign(CampaignId.example, "An example campaign", Set(TacticGroup.example), PartnerId.example)
}

//Tactic Groups names must be unique within a campaign
case class TacticGroup(id: TacticGroupId, name: Name, tactics: Set[Tactic], campaignId: CampaignId) {

}

object TacticGroup {
  val example = TacticGroup(TacticGroupId.example, "An example Tactic Group", Set(Tactic.example), CampaignId.example)
}

//tactic names must be unique within a tactic group
case class Tactic(id: TacticId, name: Name, creatives: Set[Creative], tacticGroupId: TacticGroupId) {

}

object Tactic {
  val example = Tactic(TacticId.example, "An example tactic", Set(Creative.example), TacticGroupId.example)
}

/*
A creative may be referenced by many tactics
Creative names must be unique within a tactic
A creative may have multiple titles and images attached, each unique combination yielding a CreativeInstance
The name may identify the landing page
 */
case class Creative(id: CreativeId, name: Name, landingPage: LandingPage, instances: Set[CreativeInstance], tacticIds: Set[TacticId]) {

}

object Creative {
  val example = Creative(CreativeId.example, "An example creative", LandingPage.example, Set(CreativeInstance.example), Set(TacticId.example))
}

/*
A creative instance represents a combination of title and image that are unique to a creative.
 */
case class CreativeInstance(title: Title, image: Image, creativeId: CreativeId) {
  //should this go in a companion object instead?
//  def creativeInstanceId: HashId = Hash().withinNamespace(creativeId, title, image.url.toString)
  def creativeInstanceId: HashId = Hash().string(title, image.url.toString)
}

case class Image(url: Url)

case class LandingPage(page: Page)

object LandingPage {
  val example = LandingPage(Page(Url("http://www.huffingtonpost.com/2013/04/30/wine-club-lot18_n_3165408.html").getOrElse(throw new IllegalStateException)))
}

case class Page(url: Url)

object CreativeInstance {
  val example = CreativeInstance(
    "LOT18 Tasting Room Is The First Wine Club To Get It Right"
    , Image.example
    , CreativeId.example
  )
}

object Image {
  val example = Image(
    Url("http://images.taboola.com/taboola/image/fetch/f_jpg%2Cq_80%2Ch_135%2Cw_202%2Cc_fill%2Cg_faces%2Ce_sharpen/http" +
      "%3A/cdn.taboolasyndication.com/libtrc/static/thumbnails/0b9fd3757591ed82065c760c4ab1a547.jpg")
      .getOrElse(???)
  )
}