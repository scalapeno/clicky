/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.domain.advertiser

import com.cdmt.clicky.utilities.z._
import com.cdmt.clicky.utilities.{CanBeStringSerDes, Guid, Problem}

import scala.reflect.ClassTag

/**
  *
  * Created by Unger
  *
  **/

trait GuidWrapperObj[T <: HasGuid] {

  def wrap(g: Guid): T
  def manifest: ClassTag[T]// = implicitly[ClassTag[T]] //don't call here, have to pass

  val example = wrap(Guid.exampleC(manifest))

  def apply(str: String): Problem Or T = {
    Guid(str).map(g => wrap(g))
  }

  def create(): T = {
    val g = Guid.create()
    wrap(g)
  }

  implicit val canBeStringSerDes = new CanBeStringSerDes[T] {
    override def ser(t: T): String = t.guid.toString
    override def des(str: String): Option[T] = apply(str).toOption
  }

}

trait HasGuid {
  val guid: Guid
  val id = guid.toString
}

case class OrganizationId(guid: Guid) extends HasGuid
object OrganizationId extends GuidWrapperObj[OrganizationId] {
  def manifest = implicitly[ClassTag[OrganizationId]]
  def wrap(g: Guid) = OrganizationId(g)
}

//case class AdvertiserId(guid: Guid) extends HasGuid
//object AdvertiserId extends GuidWrapperObj[AdvertiserId] {
//  def manifest = implicitly[ClassTag[AdvertiserId]]
//  def wrap(g: Guid) = AdvertiserId(g)
//}

case class CampaignId(guid: Guid) extends HasGuid
object CampaignId extends GuidWrapperObj[CampaignId] {
  def manifest = implicitly[ClassTag[CampaignId]]
  def wrap(g: Guid) = CampaignId(g)
}

case class TacticGroupId(guid: Guid) extends HasGuid
object TacticGroupId extends GuidWrapperObj[TacticGroupId] {
  def manifest = implicitly[ClassTag[TacticGroupId]]
  def wrap(g: Guid) = TacticGroupId(g)
}

case class TacticId(guid: Guid) extends HasGuid
object TacticId extends GuidWrapperObj[TacticId] {
  def manifest = implicitly[ClassTag[TacticId]]
  def wrap(g: Guid) = TacticId(g)
}

case class CreativeId(guid: Guid)

object CreativeId {

  def apply(str: String): Problem Or CreativeId = {
    Guid(str).map(g => CreativeId(g))
  }

  val example = CreativeId(Guid.exampleC(implicitly[ClassTag[CreativeId]]))

  implicit val creativeIdCanBeStringSerDes = new CanBeStringSerDes[CreativeId] {
    override def ser(t: CreativeId): String = t.guid.toString
    override def des(str: String): Option[CreativeId] = CreativeId(str).toOption
  }
}

//case class CreativeInstanceId(str: String)
//
//object CreativeInstanceId {
//  val example = CreativeInstanceId("CID321")
//
//  def apply(creativeId: CreativeId, title: Title, Image: Image) = {
//
//  }
//
//}


case class UserId(guid: Guid) extends HasGuid
object UserId extends GuidWrapperObj[UserId] {
  def manifest = implicitly[ClassTag[UserId]]
  def wrap(g: Guid) = UserId(g)
}

case class PubRuleSetId(guid: Guid) extends HasGuid
object PubRuleSetId extends GuidWrapperObj[PubRuleSetId] {
  def manifest = implicitly[ClassTag[PubRuleSetId]]
  def wrap(g: Guid) = PubRuleSetId(g)
}

case class OppId(guid: Guid) extends HasGuid
object OppId extends GuidWrapperObj[OppId] {
  def manifest = implicitly[ClassTag[OppId]]
  def wrap(g: Guid) = OppId(g)
}

case class ImpressionId(guid: Guid) extends HasGuid
object ImpressionId extends GuidWrapperObj[ImpressionId] {
  def manifest = implicitly[ClassTag[ImpressionId]]
  def wrap(g: Guid) = ImpressionId(g)
}
//case class OppId(str: String)
//
//object OppId {
//
//  val example = OppId("1A2B3C4")
//
//  implicit val opportunitydCanBeStringSerDes = new CanBeStringSerDes[OppId] {
//    override def ser(t: OppId): String = t.str
//    override def des(str: String): Option[OppId] = Option(OppId(str))
//  }
//}

//case class ImpressionId(str: String)
//
//
//object ImpressionId {
//
//  val example = ImpressionId("EECC33")
//
//  implicit val impressionIdCanBeStringSerDes = new CanBeStringSerDes[ImpressionId] {
//    override def ser(t: ImpressionId): String = t.str
//    override def des(str: String): Option[ImpressionId] = Option(ImpressionId(str))
//  }
//}

case class PartnerId(guid: Guid) extends HasGuid
object PartnerId extends GuidWrapperObj[PartnerId] {
  def manifest = implicitly[ClassTag[PartnerId]]
  def wrap(g: Guid) = PartnerId(g)
}
//case class PartnerId(str: String) extends AnyVal
//
//object PartnerId {
//  val example = PartnerId("Z9X8")
//
//  implicit val partnerIdCanBeStringSerDes = CanBeStringSerDes.mkCBSSD[PartnerId](p => {
//    p.str
//  }, str => {
//    Option(PartnerId(str))
//  })
//}
