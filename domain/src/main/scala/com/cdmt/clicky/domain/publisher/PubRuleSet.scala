/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.domain.publisher

/**
  *
  * Created by Unger
  *
  **/


import com.cdmt.clicky.utilities.Guid
import com.cdmt.clicky.utilities.ValueClassesForUtilities._

trait Enum {
  trait Type {
    def label: Label
    def index: Index
  }

  def Type(ix: Int, lab: String) = new Type {
    val label = lab.asLabel
    val index = ix.asIndex
  }

}

object Rating extends Enum {
  val G = Type(1, "G")
  val PG = Type(2, "PG")
  val PG13 = Type(3, "PG13")
  val R = Type(4, "R")
  val X = Type(4, "X")
}

object Category extends Enum {
  val Sports = Type(1, "Sports")
  val Celeb = Type(2, "Celebrity")
}

case class PubRuleSet(
                        id: Guid
                       , highestRatingAllowed: Rating.Type
                       , pubCategories: Set[Category.Type]
                       , blockedCategories: Set[Category.Type]
                     )