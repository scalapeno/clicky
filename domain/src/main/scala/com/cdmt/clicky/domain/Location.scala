/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.domain

/**
  *
  * Created by Unger
  *
  **/


import com.cdmt.clicky.utilities.ValueClassesForUtilities._
import com.cdmt.clicky.utilities.z._
import com.cdmt.clicky.utilities.{CanBeStringSerDes, Logging, Problem, ValidationProblem}
import org.joda.time.DateTimeZone

case class City(name: Name)

case class DMA(code: Int)

case class Country(name: Name, iso2: Label, iso3: Label)

case class Lat(double: Double)

case class Lon(double: Double)

case class LatLon(lat: Lat, lon: Lon)

case class Location(country: Country, city: City, dma: Option[DMA], latLon: Option[LatLon], tz: Option[DateTimeZone])


object City {

  implicit val cityCBSD = CanBeStringSerDes.mkCBSSD[City](_.name.raw, str => Some(City(str)))
}

object Country extends Logging {

  implicit val countryCBSD = CanBeStringSerDes.mkCBSSD[Country](_.name.raw, str => Country.apply(str).toOption)

  //todo: Map of countries by iso2
  def byIso2(iso2String: String): Problem Or Country = {
    CountryIsoMap.byIso2.get(iso2String) match {
      case None => {
        warn(s"Country code not found: $iso2String")
        ValidationProblem(iso2String, "Country").fail
      }
      case Some(country) => country.succ
    }
  }

  def byName(name: Name): Problem Or Country = {
    CountryIsoMap.byName.get(name) match {
      case None => {
        warn(s"Country name not found: $name")
        ValidationProblem(name, "Country").fail
      }
      case Some(country) => country.succ
    }
  }

  def apply(name: Name) = {
    byName(name)
  }

  //todo: Isn't this just hiding the fact that we don't have a proper country? Pass this back to interface?
  //On the other hand, there are very few expected instances where this would happen, and they should all be addressed.
  val empty = Country("NoCountry", "ZZ", "ZZZ") //ZZ and ZZZ reserved for this purpose

  val example = Country("United States", "US", "USA")

}

object CountryIsoMap {

  val countries = List(
    Country("United States", "US", "USA")
  )

  val byIso2 = countries.map {
    case country@Country(name, iso2, iso3) => iso2 -> country
  }.toMap

  val byIso3 = countries.map {
    case country@Country(name, iso2, iso3) => iso3 -> country
  }.toMap

  val byName = countries.map {
    case country@Country(name, iso2, iso3) => name -> country
  }.toMap

}