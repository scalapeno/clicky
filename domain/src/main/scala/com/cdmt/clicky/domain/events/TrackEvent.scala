/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.domain.events

import com.cdmt.clicky.domain.advertiser.UserId
import com.cdmt.clicky.utilities.Url
import org.joda.time.DateTime

/**
  *
  * Created by Unger
  *
  **/

case class TrackEvent (userId: UserId, url: Url, date: DateTime)

object TrackEvent {
  val example = TrackEvent(UserId.example, Url.example, new DateTime(1452986318000l))
}