/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.infrastructure.aws

/**
  *
  * Created by Unger
  *
  **/


import com.cdmt.clicky.utilities.Line
import org.scalatest.{FunSuite, Tag}

class KinesisEventStreamTest extends FunSuite {

  object ProdTest extends Tag("ProdTest")

  test("Can put something generic as Line", ProdTest) {

    val ll = Line("My^First^Record").head
    val res = KinesisEventStream().putLine(ll, "ll")
    assert(res != null)

  }

}
