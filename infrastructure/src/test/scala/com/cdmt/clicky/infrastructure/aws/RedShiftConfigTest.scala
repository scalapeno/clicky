/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.infrastructure.aws

/**
  *
  * Created by Unger
  *
  **/


import org.scalatest.FunSuite

import scala.concurrent.Await
import scala.concurrent.duration._

class RedShiftConfigTest extends FunSuite {
  import com.cdmt.clicky.infrastructure.aws.RedShiftConfig.api._

  import scala.concurrent.ExecutionContext.Implicits.global

  val user = RedShift.user
  val pwd = RedShift.pwd

  test("create dbs"){
//    val db = Database.forURL(s"jdbc:redshift://$user:$pwd@cdmt-redshift-cluster.ced0kcdo4mqq.us-east-1.redshift.amazonaws.com:5439/clicky", driver = "com.amazon.redshift.jdbc41.Driver")
    val db = Database.forConfig("redshift")
    val clicks = TableQuery[ClickEventTable]

    val click1 = ClickEventRow.example//.copy(rcvd = TimeProvider().now)
    val click2 = ClickEventRow.example//.copy(rcvd = TimeProvider().now+1)

    try {
      Await.result(db.run(DBIO.seq(
//        clicks.schema.create,

         clicks += click1
        , clicks += click2
        //        , org += OrganizationRow()

        , clicks.result.map(println)

      )), Duration.Inf)
    } catch {
      case ex: Exception => ex.printStackTrace()
    }

    info("After Catch")

//    val tables = Await.result(db.run(MTable.getTables), 1.seconds).toList
//    println(tables)
//    tables.foreach(println)

  }


}
