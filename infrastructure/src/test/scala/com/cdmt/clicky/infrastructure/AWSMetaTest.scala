/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.infrastructure

/**
  *
  * Created by Unger
  *
  **/


import com.cdmt.clicky.infrastructure.aws.AWSMeta
import org.scalatest.FunSuite

class AWSMetaTest extends FunSuite {

  test("Factory") {
    val meta = AWSMeta()
    assert(meta.isLocal)
  }



}

//object WSTest extends App {
//
//  import scala.concurrent.ExecutionContext.Implicits.global
//
//  val wsClient = NingWSClient()
//  val r = wsClient.url("http://google.com").get()
//
//  r.onComplete(t => t.map(r => println(r.body)))
//
//}
//  val request: WSRequest = ws.url("google.com")
//
//  import play.api.libs.ws.ahc._
//
//  val configString = ""
//  val rawConfig = ConfigFactory.parseString(configString)
//  val classLoader = Thread.currentThread().getContextClassLoader
//  val parser = new WSConfigParser(Configuration(rawConfig), new Environment(new File("."), classLoader, Mode.Test))
//  val clientConfig = new AhcWSClientConfig(parser.parse())
//  val builder = new AhcConfigBuilder(clientConfig)
//  val client = new AhcWSClient(builder.build())
//
//  val u = WS.url("google.com")
//  val f = u.get()
//  f.onComplete(r => r.get.body)

//}

