/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.infrastructure.rds

/**
  *
  * Created by Unger
  *
  **/



import com.cdmt.clicky.utilities.ValueClassesForUtilities._
import com.cdmt.clicky.utilities.{Guid, Logging}
import org.scalatest.{BeforeAndAfter, FunSuite}
import slick.jdbc.meta.MTable

import scala.concurrent.Await
import scala.concurrent.duration.{Duration, _}

class RDSConfigTest extends FunSuite with BeforeAndAfter with Logging {

  import com.cdmt.clicky.infrastructure.rds.RDSConfig.api._

  import scala.concurrent.ExecutionContext.Implicits.global
  // Don't import threadLocalSession, use this instead:
//  implicit var session: Session = _


  before {
//    session = Database.forURL("jdbc:h2:mem:test1", driver = "org.h2.Driver").createSession()
  }

  after {
//    session.close()
  }

  test("create dbs"){
    val db = Database.forURL("jdbc:h2:mem:test1;DB_CLOSE_DELAY=-1", driver = "org.h2.Driver")
    val org = TableQuery[OrganizationTable]

    val org1 = OrganizationRow(Guid.create(), "R org")
    val org2 = OrganizationRow(Guid.create(), "Z org")

    try {
      Await.result(db.run(DBIO.seq(
        org.schema.create

        , org += org1
        , org += org2
//        , org += OrganizationRow()

        , org.result.map(println)

      )), Duration.Inf)
    } catch {
      case ex: Exception => ex.printStackTrace()
    }

    info("After Catch")

    val tables = Await.result(db.run(MTable.getTables), 1.seconds).toList
    println(tables)
    tables.foreach(println)

  }

}

/*
  try {
    Await.result(db.run(DBIO.seq(
      // create the schema
      users.schema.create,

      // insert two User instances
      users += User("John Doe"),
      users += User("Fred Smith"),

      // print the users (select * from USERS)
      users.result.map(println)
    )), Duration.Inf)
  } finally db.close
 */