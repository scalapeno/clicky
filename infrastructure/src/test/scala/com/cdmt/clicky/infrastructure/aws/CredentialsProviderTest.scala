/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.infrastructure.aws

/**
  *
  * Created by Unger
  *
  **/


import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.auth.profile.ProfileCredentialsProvider
import com.cdmt.clicky.utilities.ValueClassesForUtilities._
import com.cdmt.clicky.utilities.z._
import org.scalatest.FunSuite

class CredentialsProviderTest extends FunSuite {

  test("Can construct") {
    val cp = CredentialsProvider()
  }

  test("Can get [default] creds from profile when they exist in the credentials file") {
    val d = CredentialsProvider().getCredsFor(AwsCreds.Default)
    assert(d.isSucc)
  }

  test("Can't get [bogus] creds from profile when not defined") {
    assertDoesNotCompile {
      """
        | val d = CredentialsProvider().getCredsFor(AwsCreds.Bogus)
      """.stripMargin}
  }

  test("Can't get creds when defined but not in file") {
    val bogus = new AwsCreds.Type {
      override def label: Label = "bogus"
      override def provider: AWSCredentialsProvider = new ProfileCredentialsProvider("bogus")
    }
    val d = CredentialsProvider().getCredsFor(bogus)
    assert(d.isFail)
  }

}
