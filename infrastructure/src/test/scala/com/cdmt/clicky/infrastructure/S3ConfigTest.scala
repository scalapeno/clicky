/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.infrastructure

/**
  *
  * Created by Unger
  *
  **/


import com.amazonaws.services.s3.AmazonS3Client
import com.cdmt.clicky.infrastructure.aws.{EventLogType, S3Config}
import com.cdmt.clicky.utilities.ValueClassesForUtilities._
import org.scalatest.FunSuite

class S3ConfigTest extends FunSuite {

}

object S3ConfigSetup extends App {

  val s3Config = new S3Config()

  val s3client = new AmazonS3Client()
  EventLogType.eventLogTypes.foreach(logType => {
    s3client.createBucket(logType.rawLogBucketName) //Uses Default US region. Not explicitly us-east?!
    s3client.createBucket(logType.archiveLogBucketName)
  })
}
