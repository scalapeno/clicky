/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.infrastructure

/**
  *
  * Created by Unger
  *
  **/


import com.cdmt.clicky.domain.IpAddress
import org.scalatest.FunSuite

class LocationProviderTest extends FunSuite {

  //todo: Real tests
  test("swaps ips for localhost") {
    val res = LocationProvider().forIpAddress(IpAddress.localhostv4)
    assert(true) //no exception thrown
  }

}
