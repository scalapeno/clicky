/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.infrastructure.aws

/**
  *
  * Created by Unger
  *
  **/

import com.cdmt.clicky.utilities.Logging
import com.typesafe.config.ConfigFactory
import slick.driver.{JdbcDriver, PostgresDriver}


object RedShiftConfig extends Logging {

  val api = profile.api

  lazy val profile: JdbcDriver = {
    sys.env.get("DB_ENVIRONMENT") match {
      case Some(env) => ConfigFactory.load().getString(s"$env.slickRedShiftDriver") match {
        //        case "scala.slick.driver.H2Driver" => {
        //          info("Bootstrapping H2 driver for RedShift")
        //          Class.forName("org.h2.Driver")
        //          H2Driver
        //        }
        case _ => { //"scala.slick.driver.Postgresql" => {
          info("Bootstrapping Redshift driver")
          Class.forName("com.amazon.redshift.jdbc41.Driver")
          PostgresDriver
        }
      }
      case _ => {
        info("No Redshift env setting found, defaulting to Postgres driver")
        Class.forName("com.amazon.redshift.jdbc41.Driver")
        PostgresDriver
      }
    }
  }
}