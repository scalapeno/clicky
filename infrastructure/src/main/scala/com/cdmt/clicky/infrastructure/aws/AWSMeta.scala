/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.infrastructure.aws

import com.cdmt.clicky.utilities.ValueClassesForUtilities._
//import com.cdmt.clicky.utilities.WebClient
//import com.cdmt.clicky.utilities.ScalaUtils._


/**
  *
  * Created by Unger
  *
  **/

trait AWSMeta {

  def isLocal: Boolean
  def instanceId: String
  def hostname: Name

}

//class AWSMetaProd extends AWSMeta {
//
//  val isLocal = false
//
//  val wc = new WebClient()
//  def awsMetaCall(url: Url) = wc.blockOnUrl(url).body
//
//  val instanceId = awsMetaCall(Url("http://169.254.169.254/latest/meta-data/instance-id"))
//  val hostname = awsMetaCall(Url("http://169.254.169.254/latest/meta-data/hostname")).asName
//
//}

class LocalAWSMeta extends AWSMeta {
  override val isLocal = true

  override val instanceId = "i-0298b2e4f1c9e7b1e"
  override val hostname = "ip-172-31-48-84.ec2.internal".asName
}

object AWSMeta {

//  val prodInstance = tryOrNone(new AWSMetaProd())
  val localInstance = new LocalAWSMeta()

  def apply() = //prodInstance getOrElse localInstance
    localInstance


}