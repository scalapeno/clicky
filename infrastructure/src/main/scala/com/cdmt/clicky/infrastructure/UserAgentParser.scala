/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.infrastructure

/**
  *
  * Created by Unger
  *
  **/


import com.cdmt.clicky.domain.ValueClassesForDomain.UserAgentHeader
import com.cdmt.clicky.domain.{DeviceInfo, DeviceType, OS}
import com.cdmt.clicky.utilities.ValueClassesForUtilities._
import eu.bitwalker.useragentutils.{DeviceType => euDT, UserAgent}

object UserAgentParser {
  val prodInstance = new UserAgentParser
  def apply() = prodInstance
}

class UserAgentParser {

  def parse(ua: UserAgentHeader) = {
    val userAgent = UserAgent.parseUserAgentString(ua.raw)

    val bitOs = userAgent.getOperatingSystem
    val os = OS(bitOs.getName)

    val bitDeviceType = userAgent.getOperatingSystem.getDeviceType
    val dt = DeviceType(bitWalkerDeviceToDeviceType.getOrElse(bitDeviceType, DeviceType.UNK))

    DeviceInfo(os, dt)
  }


  val bitWalkerDeviceToDeviceType = Map(
     euDT.COMPUTER -> DeviceType.Desktop
    ,euDT.MOBILE -> DeviceType.Mobile
    ,euDT.TABLET -> DeviceType.Tablet
    ,euDT.GAME_CONSOLE -> DeviceType.Console
    ,euDT.DMR -> DeviceType.DMR
    ,euDT.WEARABLE -> DeviceType.Wearable
    ,euDT.UNKNOWN -> DeviceType.UNK
  )

}
