/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.infrastructure.aws

/**
  *
  * Created by Unger
  *
  **/


import com.amazonaws.services.kinesis.clientlibrary.interfaces.v2.{IRecordProcessor, IRecordProcessorFactory}
import com.amazonaws.services.kinesis.clientlibrary.types.{InitializationInput, ProcessRecordsInput, ShutdownInput}
import com.cdmt.clicky.utilities.Logging

import scala.collection.JavaConversions._

object KinesisEventStreamProcessor {
  val prodInstance = new KinesisEventStreamProcessor
  def apply() = prodInstance
}

class KinesisEventStreamProcessor() extends IRecordProcessor with Logging {
  override def shutdown(shutdownInput: ShutdownInput): Unit = {
    info("Shutting down KinesisEventStreamProcessor")
  }

  override def initialize(initializationInput: InitializationInput): Unit = {
    info("KinesisEventStreamProcessor initialized")
  }

  override def processRecords(processRecordsInput: ProcessRecordsInput): Unit = {
    info(s"KinesisEventStreamProcessor processing records: ")
    processRecordsInput.getRecords.foreach(record => {
      val d = record.getData
      info(s"Data: $d")
    })
  }
}

class KinesisEventStreamProcessorFactory extends IRecordProcessorFactory with Logging {
  override def createProcessor(): IRecordProcessor = {
    info("Creating new KinesisEventStreamProcessor")
    new KinesisEventStreamProcessor
  }
}


