/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.infrastructure

/**
  *
  * Created by Unger
  *
  **/


import java.io.File
import java.net.InetAddress

import com.cdmt.clicky.domain._
import com.cdmt.clicky.utilities.ValueClassesForUtilities._
import com.cdmt.clicky.utilities.{Logging, TimeUtilities}
import com.maxmind.db.CHMCache
import com.maxmind.geoip2.DatabaseReader

object LocationProvider {
  val prodFile = {
    val classloader = Thread.currentThread().getContextClassLoader
    val url = classloader.getResource("GeoLite2-City.mmdb")
    val file = new File(url.toURI)
    file
  }

  val prodInstance = new LocationProvider(prodFile) with SwappableIps
  def apply(): LocationProvider = prodInstance
}

class LocationProvider(dbFile: File) extends Logging {

  val reader = new DatabaseReader.Builder(dbFile).withCache(new CHMCache()).build()

  def forIpAddress(ipAddress: IpAddress): Location = {

    val inetAddr = inetAddrFor(ipAddress)

    val mmCity = reader.city(inetAddr)

    val country = Country.byIso2(mmCity.getCountry.getIsoCode).getOrElse{
      warn(s"Couldn't find iso2 country for: ${mmCity.getCountry.getIsoCode}")
      Country.empty
    }

    val city = City(mmCity.getCity.getName)

    val dma = Option(DMA(mmCity.getLocation.getMetroCode))

    val lat = Lat(mmCity.getLocation.getLatitude)
    val lon = Lon(mmCity.getLocation.getLongitude)
    val latLon = Option(LatLon(lat, lon))

    //Timezone is optional
    val tz = TimeUtilities.tzForId(mmCity.getLocation.getTimeZone)

    Location(country, city, dma, latLon, tz)
  }

  protected def inetAddrFor(ipAddress: IpAddress): InetAddress = InetAddress.getByName(ipAddress.toString)

}

trait SwappableIps extends Logging {
  self: LocationProvider =>

  override def inetAddrFor(ipAddress: IpAddress): InetAddress = {

    if(ipAddress.isLocalhost) {
      warn(s"Swapping fake IP for localhost ip")
      InetAddress.getByName(IpAddress.example.toString)
    }
    else {
      InetAddress.getByName(ipAddress.toString)
    }


  }
}