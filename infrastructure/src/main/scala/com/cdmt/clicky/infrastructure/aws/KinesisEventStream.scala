/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.infrastructure.aws

/**
  *
  * Created by Unger
  *
  **/


import java.nio.ByteBuffer

import com.amazonaws.services.kinesis.AmazonKinesisClient
import com.amazonaws.services.kinesis.model.PutRecordRequest
import com.cdmt.clicky.utilities.Line
import com.cdmt.clicky.utilities.ValueClassesForUtilities._

object KinesisEventStream {
  //per viktor klang, java.util.concurrent.Future is an abomination
  //http://stackoverflow.com/questions/11529145/how-do-i-wrap-a-java-util-concurrent-future-in-an-akka-future
  //todo: Hide this blocking behind an actor
  val prodInstance = new KinesisEventStream(new AmazonKinesisClient(), "event-stream")
  def apply() = prodInstance
}

class KinesisEventStream(client: AmazonKinesisClient, streamName: Name) {

//  asyncClient.get
//  def put[T: CanBeLine: HasPartitionKey](t: T) = {
//    val line = CanBeLine.toLine(t)
//    val pKey = HasPartitionKey.getKey(t)
//    putLine(line, pKey)
//  }

  def putLine(line: Line, partitionKey: String) = {
    val bb = line.toByteBuffer
    putByteBuffer(bb, partitionKey)
  }

  def putByteBuffer(bb: ByteBuffer, partitionKey: String) = {
    val req = new PutRecordRequest()
    req.setStreamName(streamName)
    req.setData(bb)
    req.setPartitionKey(partitionKey)


    val res = client.putRecord(req)
    res
  }

}
