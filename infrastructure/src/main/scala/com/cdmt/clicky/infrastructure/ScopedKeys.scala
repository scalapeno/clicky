/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.infrastructure

/**
  *
  * Created by Unger
  *
  **/

import com.cdmt.clicky.domain.advertiser.PartnerId
import com.cdmt.clicky.utilities.ScopedKey

object ScopedKeys {


  case class PartnerKey(key: PartnerId) extends ScopedKey[PartnerId] {
    val scope = ScopedKey.Type.Partner
  }

}
