/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.infrastructure.aws

import com.amazonaws.services.s3.AmazonS3Client
import com.cdmt.clicky.domain.events._
import com.cdmt.clicky.utilities.Logging
import com.cdmt.clicky.utilities.ValueClassesForUtilities._
import org.joda.time.DateTime

/**
  *
  * Created by Unger
  *
  **/

object S3Config {

  val bucketKeyTimeLock = new Object()
  var keyCount = 0

  val instance = new S3Config()

  def apply() = instance

}

class S3Config() extends Logging {
//  val rawLogBuckets = Map(
//    "requestRawLogs" -> "logs-raw-requests"
//    ,"bidRawLogs" -> "logs-raw-bids"
//    ,"winRawLogs" -> "logs-raw-wins"
//    ,"renderRawLogs" -> "logs-raw-renders"
//    ,"viewRawLogs" -> "logs-raw-views"
//    ,"clickRawLogs" -> "logs-raw-clicks"
//    ,"conversionRawLogs" -> "logs-raw-conversion"
//  )
//
//  val archiveLogBuckets = Map(
//    "requestArchiveLogs" -> "logs-archive-requests"
//    ,"bidArchiveLogs" -> "logs-archive-bidss"
//    ,"winArchiveLogs" -> "logs-archive-wins"
//    ,"renderArchiveLogs" -> "logs-archive-renders"
//    ,"viewArchiveLogs" -> "logs-archive-views"
//    ,"clickArchiveLogs" -> "logs-archive-clicks"
//    ,"conversionArchiveLogs" -> "logs-archive-conversion"
//  )

  def hostName = AWSMeta().hostname

  def makeKeyName() = S3Config.bucketKeyTimeLock.synchronized {
    //Do we throw something else in here in case there are two threads writing at the same milli?
    //synchronize instead. That doesnt work, can unlock within the same milli.
    val keyName = s"${hostName.raw}-${S3Config.keyCount}-${new DateTime().getMillis}.txt"
    S3Config.keyCount = S3Config.keyCount + 1
    keyName
  }

//  def makeKeyNameSingleKeys() = {
//
//  }

}

case class EventLogType[T](example: T, label: Label, rawLogBucketName: Name, archiveLogBucketName: Name)

object EventLogType {

  val trackEvent = EventLogType(TrackEvent.example, "trackEvent".asLabel, "cdmt-logs-raw-tracks".asName, "cdmt-logs-archive-tracks".asName)
  val opportunityEvent = EventLogType(OpportunityEvent.example, "opportunityEvent".asLabel, "cdmt-logs-raw-opps".asName, "cdmt-logs-archive-opps".asName)
//  val bidEvent = EventLogType(BidEvent.example, "bidEvent".asLabel, "cdmt-logs-raw-bids".asName, "cdmt-logs-archive-bids".asName)
//  val noBidEvent = EventLogType(NoBidEvent.example, "noBidEvent".asLabel, "cdmt-logs-raw-nobids".asName, "cdmt-logs-archive-nobids".asName)
//  val winEvent = EventLogType(WinEvent.example, "winEvent".asLabel, "cdmt-logs-raw-wins".asName, "cdmt-logs-archive-wins".asName)
  val impressionServedEvent = EventLogType(ImpressionServedEvent.example, "ImpressionServedEvent".asLabel, "cdmt-logs-raw-impression-serveds".asName, "cdmt-logs-archive-impression-serveds".asName)
//  val unfilledEvent = EventLogType(UnfilledEvent.example, "unfilledEvent".asLabel, "cdmt-logs-raw-unfilleds".asName, "cdmt-logs-archive-unfilleds".asName)
//  val renderEvent = EventLogType(RenderEvent.example, "renderEvent".asLabel, "cdmt-logs-raw-renders".asName, "cdmt-logs-archive-renders".asName)
//  val viewEvent = EventLogType(ViewEvent.example, "viewEvent".asLabel, "cdmt-logs-raw-views".asName, "cdmt-logs-archive-views".asName)
  val clickEvent = EventLogType(ClickEvent.example, "clickEvent".asLabel, "cdmt-logs-raw-clicks".asName, "cdmt-logs-archive-clicks".asName)
//  val conversionEvent = EventLogType(ConversionEvent.example, "conversionsEvent".asLabel, "cdmt-logs-raw-conversions".asName, "cdmt-logs-archive-conversions".asName)

  val eventLogTypes = List(
    trackEvent
    , opportunityEvent
//    , bidEvent
//    , noBidEvent
//    , winEvent
    , impressionServedEvent
//    , unfilledEvent
//    , renderEvent
//    , viewEvent
    , clickEvent
//    , conversionEvent
  )

}

object BuildS3Buckets extends App {
  val s3Client = new AmazonS3Client()

  EventLogType.eventLogTypes.foreach(typ => {
    s3Client.createBucket(typ.rawLogBucketName)
    s3Client.createBucket(typ.archiveLogBucketName)
  })

}
