/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.infrastructure.rds

import com.cdmt.clicky.utilities.Logging
import com.typesafe.config.ConfigFactory
import slick.driver.{H2Driver, JdbcDriver, PostgresDriver}


object RDSConfig extends Logging {
  val api = profile.api
  lazy val profile: JdbcDriver = {
    sys.env.get("DB_ENVIRONMENT") match {
      case Some(e) => ConfigFactory.load().getString(s"$e.slickDriver") match {
        case "scala.slick.driver.H2Driver" => {
          info("Bootstrapping H2 driver")
          Class.forName("org.h2.Driver")
          H2Driver
        }
        case "scala.slick.driver.MySQLDriver" => PostgresDriver
      }
      case _ => H2Driver
    }
  }
}