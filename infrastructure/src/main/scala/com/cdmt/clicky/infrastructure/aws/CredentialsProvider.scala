/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.infrastructure.aws

/**
  *
  * Created by Unger
  *
  **/


import com.amazonaws.auth.profile.ProfileCredentialsProvider
import com.amazonaws.auth.{AWSCredentials, AWSCredentialsProvider}
import com.cdmt.clicky.utilities.ValueClassesForUtilities._
import com.cdmt.clicky.utilities.z._
import com.cdmt.clicky.utilities.{CredentialsProblem, Problem}

object CredentialsProvider {
  val prodInstance = new CredentialsProvider()
  def apply() = prodInstance
}

class CredentialsProvider {

  //todo: This is very static. Wouldn't it be nicer to dynamically supply the credential name instead of hardcode?

  def getCredsFor(typ: AwsCreds.Type): Problem Or AWSCredentials = {
    val provider = typ.provider

    val creds = try {
      provider.getCredentials.succ[Problem]
    } catch {
      case awsEx: Exception => {
        CredentialsProblem("Couldn't find credentials", Some(awsEx)).fail
      }
    }
    creds
  }

}

object AwsCreds {

  trait Type {
    def label: Label
    def provider: AWSCredentialsProvider
  }

  object Type {
    def mkType(lab: String, prov: AWSCredentialsProvider) = new Type {
      val label = lab.asLabel
      val provider = prov
    }
  }

  val Kinesis = Type.mkType("kinesis", new ProfileCredentialsProvider("kinesis"))
  val Default = Type.mkType("default", new ProfileCredentialsProvider("default"))

}