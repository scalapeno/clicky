/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.infrastructure.aws

/**
  *
  * Created by Unger
  *
  **/


import com.cdmt.clicky.domain.ValueClassesForDomain._
import com.cdmt.clicky.domain._
import com.cdmt.clicky.domain.advertiser.{ImpressionId, OppId, PartnerId, UserId}
import com.cdmt.clicky.domain.events.ClickEvent
import com.cdmt.clicky.infrastructure.aws.RedShiftConfig.api._
import com.cdmt.clicky.infrastructure.{LocationProvider, UserAgentParser}
import com.cdmt.clicky.utilities.ValueClassesForUtilities._
import com.cdmt.clicky.utilities.{CanBeStringSerDes, Logging, TimeProvider, TimeUtilities}
import org.joda.time.DateTimeZone

import scala.concurrent.Await
import scala.concurrent.duration.Duration


object RedShift{
  val prodInstance = new RedShift()
  def apply() = prodInstance

  val user = "CDMT_clicky"
  val pwd = "C1!ckY_C33D33EmTee"

}

class RedShift() {
  def persist(ce: ClickEvent): Unit = {
    val row = mkClickEventRow(ce)
    writeClickEventRow(row)
  }

  def mkClickEventRow(ce: ClickEvent) = {
    //todo: Store per-pub and per-adv timezones
    val tzForPub: DateTimeZone = DateTimeZone.UTC
    val tzForAdv: DateTimeZone = DateTimeZone.UTC
    val tzForUser: DateTimeZone = DateTimeZone.UTC

    val location = LocationProvider().forIpAddress(ce.ip)
    val deviceInfo = UserAgentParser().parse(ce.ua)

    val row = ClickEventRow(
      ce.opp
      , ce.imp
      , ce.partnerId
      , ce.ppid
      , ce.userId
      , TimeUtilities.utcYear(ce.rcvd) //todo: Update ClickEvent model so that this logic is useful at the beginning of the ClickEvent lifecycle, not just at the end.
      , TimeUtilities.utcDay(ce.rcvd)
      , TimeUtilities.utcHour(ce.rcvd)
      , ce.rcvd
      , TimeUtilities.dayForTimezone(ce.rcvd, tzForPub)
      , TimeUtilities.hourForTimezone(ce.rcvd, tzForPub)
      , TimeUtilities.dayForTimezone(ce.rcvd, tzForAdv)
      , TimeUtilities.hourForTimezone(ce.rcvd, tzForAdv)
      , location.tz.map(tz => TimeUtilities.dayForTimezone(ce.rcvd, tz))
      , location.tz.map(tz => TimeUtilities.hourForTimezone(ce.rcvd, tzForUser))
      , ce.ip
      , location.country
      , location.city
      , ce.ua
      , deviceInfo.deviceType
      , deviceInfo.os
    )

    row
  }

  def writeClickEventRow(ceRow: ClickEventRow) = {
    val db = Database.forConfig("redshift")
    val clicks = TableQuery[ClickEventTable]

    try {
      Await.result(db.run(DBIO.seq(

        //Insert into clicks table
        clicks += ceRow

      )), Duration.Inf)
    } catch {
      case ex: Exception => ex.printStackTrace()
    }
  }

}

/*
  The goal is to be able to write metrics with a timestamp and then aggregate them by a flexible definition
  of which millis belong in a given day.

  So for instance, in Greenwich, Millis(1458165992247) represents day 77
  but in Los Angeles, Millis(1458165992247) represents day 76.
  So, do we store this as day 77 UTC
  or day 76 Los Angeles,
  or both?

  I guess we're doing both for now.
   */
case class ClickEventRow(oppId: OppId
                         , impId: ImpressionId
                         , partnerId: PartnerId
                         , ppid: PPID
                         , userId: UserId
                         , receivedYearUTC: Year
                         , receivedDayUTC: Day
                         , receivedHourUTC: Hour
                         , receivedMillis: Millis
                         , pubDay: Day
                         , pubHour: Hour
                         , advDay: Day
                         , advHour: Hour
                         , userDay: Option[Day]
                         , userHour: Option[Hour]
                         , ip: IpAddress
                         , country: Country
                         , city: City
                         , ua: UserAgentHeader
                         , device: DeviceType
                         , os: OS
                        )

object ClickEventRow {
  val example = ClickEventRow(OppId.example, ImpressionId.example, PartnerId.example, PPID.example, UserId.example
    , Year(2016), Day(76), Hour(15), TimeProvider().now, Day(75), Hour(22), Day(76), Hour(1), Option(Day(76)), Option(Hour(4)), IpAddress.example
  , Country.example, City("Los Angeles"), UserAgentHeader("mobile"), DeviceType.example, OS.example)

  def fromTuple(tup: (String, String, String, String, String, Int, Int, Int, Long, Int, Int, Int, Int, Option[Int], Option[Int], String, String, String, String, String, String)) = {//, String, String, String, Int, Int, Int, Int, Long, String, String, String, String, String, String)) = {

    val (oppId, impId, partnerId, ppid, userId, receivedYearUTC, receivedDayUTC, receivedHourUTC, receivedMillis, pubDay, pubHour, advDay, advHour, userDay, userHour, ip, country, city, ua, device, os) = tup

    //todo: All of these .gets. Should be?
    ClickEventRow(
        CanBeStringSerDes.des[OppId](oppId).get
      , CanBeStringSerDes.des[ImpressionId](impId).get
      , CanBeStringSerDes.des[PartnerId](partnerId).get
      , CanBeStringSerDes.des[PPID](ppid).get
      , CanBeStringSerDes.des[UserId](userId).get
      , Year(receivedYearUTC)
      , Day(receivedDayUTC)
      , Hour(receivedHourUTC)
      , Millis(receivedMillis)
      , Day(pubDay)
      , Hour(pubHour)
      , Day(advDay)
      , Hour(advHour)
      , userDay.map(Day)
      , userHour.map(Hour)
      , CanBeStringSerDes.des[IpAddress](ip).get
      , CanBeStringSerDes.des[Country](country).get
      , CanBeStringSerDes.des[City](city).get
      , CanBeStringSerDes.des[UserAgentHeader](ua).get
      , CanBeStringSerDes.des[DeviceType](device).get
      , CanBeStringSerDes.des[OS](os).get
    )
  }

  def toTuple(ceRow: ClickEventRow) = {
    Option((
      CanBeStringSerDes.ser(ceRow.oppId)
      ,CanBeStringSerDes.ser(ceRow.impId)
      ,CanBeStringSerDes.ser(ceRow.partnerId)
      ,CanBeStringSerDes.ser(ceRow.ppid)
      ,CanBeStringSerDes.ser(ceRow.userId)
      ,ceRow.receivedYearUTC.raw
      ,ceRow.receivedDayUTC.raw
      ,ceRow.receivedHourUTC.raw
      ,ceRow.receivedMillis.raw //todo: Is it better to just throw all strings into Redshift to simplify serdes?
      ,ceRow.pubDay.raw
      ,ceRow.pubHour.raw
      ,ceRow.advDay.raw
      ,ceRow.advHour.raw
      ,ceRow.userDay.map(_.raw)
      ,ceRow.userHour.map(_.raw)
      ,CanBeStringSerDes.ser(ceRow.ip)
      ,CanBeStringSerDes.ser(ceRow.country)
      ,CanBeStringSerDes.ser(ceRow.city)
      ,CanBeStringSerDes.ser(ceRow.ua)
      ,CanBeStringSerDes.ser(ceRow.device)
      ,CanBeStringSerDes.ser(ceRow.os)
    ))
  }  
  
}

class ClickEventTable(tag: Tag) extends Table[ClickEventRow](tag, "ClickEvents") with Logging {
  def oppId = column[String]("oppid", O.PrimaryKey)
  def impId = column[String]("impid")
  def partnerId = column[String]("partnerid")
  def ppid = column[String]("ppid")
  def userId = column[String]("userid")
  def receivedYear = column[Int]("receivedyear")
  def receivedDay = column[Int]("receivedday")
  def receivedHour = column[Int]("receivedhour")
  def receivedMillis = column[Long]("receivedmillis")
  def pubDay = column[Int]("pubday")
  def pubHour = column[Int]("pubhour")
  def advDay = column[Int]("advday")
  def advHour = column[Int]("advhour")
  def userDay = column[Option[Int]]("userday")
  def userHour = column[Option[Int]]("userhour")
  def ipAddress = column[String]("ipaddress")
  def country = column[String]("country")
  def city = column[String]("city")
//  def latlon = column[String]("latlon")
  def userAgent = column[String]("useragent")
  def deviceType = column[String]("devicetype")
  def os = column[String]("os")

//  override def * = (oppId, impId, partnerId, ppid, userId, receivedYear, receivedMonth, receivedDay, receivedHour, receivedMillis, ipAddress, userAgent) <> (ClickEventRow.fromTuple, ClickEventRow.toTuple)

  override def * = (oppId, impId, partnerId, ppid, userId, receivedYear, receivedDay, receivedHour, receivedMillis, pubDay, pubHour, advDay, advHour, userDay, userHour, ipAddress, country, city, userAgent, deviceType, os) <> (ClickEventRow.fromTuple, ClickEventRow.toTuple)
}
