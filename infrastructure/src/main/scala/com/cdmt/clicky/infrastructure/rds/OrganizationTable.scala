/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.infrastructure.rds

/**
  *
  * Created by Unger
  *
  **/

import com.cdmt.clicky.infrastructure.rds.RDSConfig.api._
import com.cdmt.clicky.utilities.ValueClassesForUtilities._
import com.cdmt.clicky.utilities.{Guid, Logging}

case class OrganizationRow(id: Guid, name: Name)

class OrganizationTable(tag: Tag) extends Table[OrganizationRow](tag, "ORGANIZATION") with Logging {
  def id = column[String]("id", O.PrimaryKey)
  def name = column[String]("name")

  val toRow = (tup: (String, String)) => {
    val (id: String, name: String) = (tup._1, tup._2)

    val orgGuid = Guid(id).getOrElse {
      warn(s"DB contained invalid Guid!")
      throw new IllegalStateException("Invalid Guid")
    }

    OrganizationRow(orgGuid, name)
  }

  val fromRow = (orgRow: OrganizationRow) => {
    Option(orgRow.id.toString, orgRow.name.raw)
  }

  override def * = (id, name) <> (toRow, fromRow)

}