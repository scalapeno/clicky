package com.cdmt.clicky.infrastructure.rds

import com.cdmt.clicky.utilities.ValueClassesForUtilities.Name
import com.cdmt.clicky.utilities.{Guid, Logging}

/**
  * Created by randy on 6/22/16.
  */

import com.cdmt.clicky.infrastructure.rds.RDSConfig.api._

case class PartnerRow(id: Guid, name: Name, organization: Guid)

class PartnerTable(tag: Tag) extends Table[PartnerRow](tag, "PARTNER") with Logging {
  def id = column[String]("id", O.PrimaryKey)
  def name = column[String]("name")
  def organizationId = column[String]("organizationId")

  val toRow = (tup: (String, String, String)) => {
    val (id: String, name: String, organizationId: String) = (tup._1, tup._2, tup._3)

    val guid = Guid(id).getOrElse {
      warn(s"DB contained invalid Guid!")
      throw new IllegalStateException("Invalid Guid")
    }

    //Share this
    val pGuid = Guid(id).getOrElse {
      warn(s"DB contained invalid Guid!")
      throw new IllegalStateException("Invalid Guid")
    }

    PartnerRow(guid, name, pGuid)
  }

  val fromRow = (row: PartnerRow) => {
    Option(row.id.toString, row.name.raw, row.organization.toString)
  }

  override def * = (id, name, organizationId) <> (toRow, fromRow)

}