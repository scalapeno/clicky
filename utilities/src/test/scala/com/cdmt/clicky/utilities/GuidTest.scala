/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.utilities

/**
  *
  * Created by Unger
  *
  **/


import com.cdmt.clicky.utilities.z._
import org.scalatest.FunSuite

import scala.reflect.ClassTag

class GuidTest extends FunSuite {

  test("Distribution"){

    (1 to 100).map(i => {
      println((i, Guid.create()))
    })


  }

  test("String rep") {

    val n = Guid.create()
    println(n.toString)

  }

  test("factory rejects too short strings"){
    val badStr = "c28c"
    val badGuid = Guid(badStr)
    assert(badGuid.isFail)
  }


  test("factory rejects invalid letters"){
    val badStr = "ABCDEFGHIJKL"
    val badGuid = Guid(badStr)
    info(badGuid.toString)
    assert(badGuid.isFail)
  }

  test("add dashes only") {
    val start = "12345678901234567890123456789012"
    val d = Guid.addDashes(start)
    println(d)
    assert(d.replace("-", "") == start)
  }

  test("dashes") {
    val g = Guid.create()
    val ustr = g.uuid.toString
    println(ustr)
    val gstr = g.toString
    println(gstr)
    assert(gstr == ustr.replace("-", ""))

    val newG = Guid(gstr)
    println(newG)
    assert(newG.isSucc)

    val g2Str = newG.getOrElse(???).toString
    val g2 = Guid(g2Str)

    assert(g2.isSucc)
    assert(g2 == newG)
  }

  test("factory accepts valid strings"){
    val goodStr = "828c5ae9916d4a5488a70e2279a4530e"
    val goodGuid = Guid(goodStr)
    assert(goodGuid.isSucc)
  }


  test("String to guid twice is the same each time"){
    val goodStr = "828c5ae9916d4a5488a70e2279a4530e"
    val goodGuidV = Guid(goodStr)
    goodGuidV match {
      case Fail(_) => assert(goodGuidV.isSucc) //Wrong
      case Succ(guid) => {
        val newGuid = Guid(goodStr)
        assert(goodGuidV == newGuid)
      }
    }
  }

  test("consistent example with type param") {
    val ex1 = Guid.exampleC(implicitly[ClassTag[String]])
    println(s"str: $ex1")

    val ex2 = Guid.exampleC(implicitly[ClassTag[Int]])
    println(s"int: $ex2")

    assert(ex1 != ex2)

    val ex3 = Guid.exampleC(implicitly[ClassTag[String]])
    println(s"string: $ex3")
    assert(ex1 == ex3)

  }

  test("ExampleT works when invoked from Guid Wrapper") {
    trait GuidWrapperTest[T] {
      def ct: ClassTag[T]
      def wrap(g:Guid): T
      val example = wrap(Guid.exampleC(ct))
    }

    case class OO()

    val OObj = new GuidWrapperTest[OO] {
      override def ct = implicitly[ClassTag[OO]]
      override def wrap(g:Guid) = new OO
    }

    val ex1 = OObj.example
    val ex2 = OObj.example

    assert(ex1 == ex2)

  }

//  test("Converts back and forth per example longs"){
//    val goodStr = "828c5ae9916d4a5488a70e2279a4530e"
//    val goodGuidV = Guid(goodStr)
//    goodGuidV match {
//      case Failure(_) => assert(goodGuidV.isSuccess) //Wrong
//      case Success(guid) => {
//        assert(Guid.exampleLongs._1 == hi)
//        assert(Guid.exampleLongs._2 == lo)
//      }
//    }
//  }

}
