/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.utilities

/**
  *
  * Created by Unger
  *
  **/


import org.scalatest.FunSuite

class zTest extends FunSuite {

  test("Disjunction alias"){
    import scalaz._

    def validate(input: String): Disjunction[IllegalArgumentException, String] = input match {
      case "fail" => Disjunction.left(new IllegalArgumentException)
      case other => Disjunction.right("other")
    }

    val vFail = validate("fail")
    assert(vFail.isLeft)

    val vPass = validate("pass")
    assert(vPass.isRight)
  }

  test("better syntax"){
    import z._

    def validate(input: String) = input match {
      case "fail" => (new IllegalArgumentException).fail
      case other => "other".succ
    }

    val vFail = validate("fail")
    assert(vFail.isFail)

    val vPass = validate("pass")
    assert(vPass.isSucc)
  }

  test("Unapply, single import, infix Or") {
    import z._

    def validate(input: String) = input match {
      case "fail" => (new IllegalArgumentException).fail
      case other => input.succ
    }

    def proc(s: Exception Or String) = s match {
      case Fail(ex) => s"$s was NOT valid: $ex"
      case Succ(ss) => s"$s was valid: $ss"
    }

    val pp = proc(validate("pass"))
    println(pp)

    val ff = proc(validate("fail"))
    println(ff)
  }

  test("chained Ors") {
    import z._
    def pp(v: Exception Or Int Or String) = {
      v match {
        case Fail(Fail(ex)) => println(s"Exception: $ex")
        case Fail(Succ(i)) => println(s"Int: $i")
        case Succ(s) => println(s"String: $s")
      }
    }

    val v = 5.succ[Exception].fail[String]
    pp(v)

  }


}

