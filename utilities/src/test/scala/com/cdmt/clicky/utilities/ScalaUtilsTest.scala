/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.utilities

/**
  *
  * Created by Unger
  *
  **/


import com.cdmt.clicky.utilities.ScalaUtils._
import org.scalatest.FunSuite

class ScalaUtilsTest extends FunSuite {

  test("tryOrNone"){
    assert(tryOrNone(throw new Exception).isEmpty)
    assert(tryOrNone(55).isDefined)
  }

}