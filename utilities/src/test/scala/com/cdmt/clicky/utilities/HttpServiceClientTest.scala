package com.cdmt.clicky.utilities

import org.scalatest.FunSuite

/**
  * Created by randy on 4/22/16.
  */
class HttpServiceClientTest extends FunSuite {

  test("Http Client to google"){
//    val cl = new HttpServiceClient("google.com:80")
    val cl = new HttpServiceClient()
    val resp = cl.blockingRequest("http://google.com:80/")
    println(resp.parseAsString())
    assert(resp.getStatusCode()==200)
  }

  test("Http Client for localhost"){
//    val cl = new HttpServiceClient("localhost:8080")
    val cl = new HttpServiceClient()
    val resp = cl.blockingRequest("http://localhost:8080/")
    println(resp.parseAsString())
    assert(resp.getStatusCode()==200)
  }

}
