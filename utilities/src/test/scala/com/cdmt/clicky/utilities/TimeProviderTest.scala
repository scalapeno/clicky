/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.utilities

/**
  *
  * Created by Unger
  *
  **/


import com.cdmt.clicky.utilities.ValueClassesForUtilities._
import org.scalatest.FunSuite

class TimeProviderTest extends FunSuite {

  test("that default impl fails to provide injection") {
    val defaultTp = TimeProvider() //This impl should fail to provide idempotency

    val t1 = defaultTp.now
    println(s"t1: $t1")

    Thread.sleep(100)

    val t2 = defaultTp.now
    println(s"t2: $t2")

    assert(t1 != t2)
  }


  test("test that Now can be injected") {
    //Required for idempotent testing
    val injectedTP = new TimeProvider(0)

    val t1 = injectedTP.now
    println(s"t1: $t1")

    Thread.sleep(100)

    val t2 = injectedTP.now
    println(s"t2: $t2")

    assert(t1 == t2)

  }

  test("that injected factory is called each time") {
    var t = 0
    val interatingTP = new TimeProvider({t = t+1; t})

    val t1 = interatingTP.now
    println(s"t1: $t1")

    Thread.sleep(100)

    val t2 = interatingTP.now
    println(s"t2: $t2")

    assert(t2 > t1)
  }

}
