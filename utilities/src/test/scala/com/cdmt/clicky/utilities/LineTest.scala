/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.utilities

/**
  *
  * Created by Unger
  *
  **/


import org.scalatest.FunSuite

class LineTest extends FunSuite {

  val lineA = Line("aaaaa")
  val lineB = Line("bbbb")
  val lineSeq = List(lineA, lineB).flatten
  val lineFile = LineFile(lineSeq)

  test("testToInputStream") {

  }

  test("testString") {
    assert(lineFile.string == "aaaaa\nbbbb")
  }

  test("Tokens"){
    val str = "089a6fd5066b4669a44c4954842d8305^3e87c2150c3d443e9fdf22f0e624dfc8^00000000000000000000001484028668^123~456-AB_CDE^00000000000000000000001879063974^3343636080132341166"
//    str.split("\\^")
    val line = Line(str).head
    val tokens = line.tokens
    assert(tokens.length == 6)
  }

}

