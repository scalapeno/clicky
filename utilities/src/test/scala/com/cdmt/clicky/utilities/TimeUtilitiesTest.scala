/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.utilities

/**
  *
  * Created by Unger
  *
  **/


import com.cdmt.clicky.utilities.ValueClassesForUtilities._
import org.joda.time.{DateTime, DateTimeZone}
import org.scalatest.FunSuite

class TimeUtilitiesTest extends FunSuite {

  test("Year") {
    val millis = Millis(1458165992247l)
    println(s"Millis: $millis")

    val year = TimeUtilities.utcYear(millis)
    println(s"Year: $year")

    assert(year == Year(2016))
  }

  test("Day") {
    val millis = Millis(1458165992247l)
    println(s"Millis: $millis")

    val day = TimeUtilities.utcDay(millis)
    println(s"Year: $day")

    assert(day == Day(76))
  }

  test("Hour") {
    val millis = Millis(1458165992247l)
    println(s"Millis: $millis")

    val hour = TimeUtilities.utcHour(millis)
    println(s"Hour: $hour")

    assert(hour == Hour(22))
  }

  test("Day for timezone: LA") {
    val millis = Millis(1458165992247l)
    println(s"Millis: $millis")

    val tz = DateTimeZone.forID("US/Pacific")
    val day = TimeUtilities.dayForTimezone(millis, tz)
    println(s"Day: $day")

    assert(day == Day(76))
  }

  test("Hour for timezone: LA") {
    val millis = Millis(1458165992247l)
    println(s"Millis: $millis")

    //DateTimeZone.getAvailableIDs.forEach(println)
    val tz = DateTimeZone.forID("US/Pacific")
    val hour = TimeUtilities.hourForTimezone(millis, tz)
    println(s"Hour: $hour")

    assert(hour == Hour(15))
  }

  test("Timestamp near boundary is different day in different timezones") {

    val pac = DateTimeZone.forID("US/Pacific")
    val east = DateTimeZone.forID("US/Eastern")

    val dt = new DateTime(2016, 2,2,0,0, east)

    val dayEast = TimeUtilities.dayForTimezone(dt.getMillis, east)
    val dayWest = TimeUtilities.dayForTimezone(dt.getMillis, pac)

    println(s"DayEast: $dayEast")
    println(s"DayPac: $dayWest")

    assert(dayEast.raw - dayWest.raw == 1)
  }






}
