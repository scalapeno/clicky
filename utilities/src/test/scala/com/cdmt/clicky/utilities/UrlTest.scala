/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.utilities

/**
  *
  * Created by Unger
  *
  **/


import com.cdmt.clicky.utilities.z._
import org.scalatest.FunSuite

class UrlTest extends FunSuite {

  test("Factory as expected") {
    val a = Url("http://images.taboola.com/taboola/image/fetch/f_jpg%2Cq_80%2Ch_135%2Cw_202%2Cc_fill%2Cg_faces%2Ce_sharpen/http%3A/cdn.taboolasyndication.com/libtrc/static/thumbnails/0b9fd3757591ed82065c760c4ab1a547.jpg")

    val b = Url("http://www.huffingtonpost.com/2013/04/30/wine-club-lot18_n_3165408.html")

    assert(a.isRight)
    assert(b.isRight)
  }

  test("base 64 param"){
    val c = "http://localhost:8080/click?cl=dY3LDUIxEMQqQspmJrObI782gpJHtgxqB-7gmyVLtvSdQs_UpqfHTHA2_5oO5Yjn4VzVSl9OqcRcPktytR7bc4_yE2NYdNI0rOLFptP58rje7v96hPeAeRsAIXxOxVBBM-kN"

    val u = Url(c)

    assert(u.isSucc)
  }



}
