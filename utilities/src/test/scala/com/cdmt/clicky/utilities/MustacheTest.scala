package com.cdmt.clicky.utilities

import com.github.jknack.handlebars.Handlebars
import com.github.jknack.handlebars.io.URLTemplateSource
import org.scalatest.FunSuite

/**
  * Created by randy on 4/27/16.
  */
class MustacheTest extends FunSuite {

  test("Mustache") {
    val handleBars = new Handlebars()
    val classloader = Thread.currentThread().getContextClassLoader
    val url = classloader.getResource("hello.mustache")
//    val templateName = "hello.mustache"
//    val url = new URL(s"file://$templatePath/$templateName")
    val src = new URLTemplateSource("aTest", url)
//    val template = handleBars.compile()
    val out = handleBars.compile(src)
//    out.toJavaScript
//    println(out.text())
    out.apply()

  }

}
