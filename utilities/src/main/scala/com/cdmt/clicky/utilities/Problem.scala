/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.utilities

/**
  *
  * Created by Unger
  *
  **/


import com.cdmt.clicky.utilities.ValueClassesForUtilities._
import com.cdmt.clicky.utilities.z._

class Problem(val message: Text, val throwable: Option[Throwable]) {

  def canEqual(other: Any): Boolean = other.isInstanceOf[Problem]

  override def equals(other: Any): Boolean = ???  //You probably shouldn't be doing this

  override def hashCode(): Int = ???

  override def toString = s"Problem($message, $throwable)"
}

object Problem {

  def apply(message: Text, throwable: Option[Throwable]): Problem = new Problem(message, throwable)

  def apply(message: Text, exception: Throwable): Problem = {
    Problem(message, Some(exception))
  }

  def apply(message: Text): Problem = {
    Problem(message, None)
  }

  def apply(problems: NonEmptyList[Problem]): Problem = {
    val head = "Multiple failures: "
    val mid = problems.list.toList.mkString(",")
    MultipleProblems(problems.list.toList, head + mid, None)
  }

}

object ValidationProblem {

  def mkMsg(input: String, destination: Label) = Text(s"Validation failed for $input as $destination")
}

case class ValidationProblem(input: String, destination: Label) extends Problem(ValidationProblem.mkMsg(input, destination), None)

case class CredentialsProblem(msg: Text, ex: Option[Throwable]) extends Problem(msg, ex)

case class MysteriousProblem(msg: Text, ex: Option[Throwable]) extends Problem(msg, ex)

case class MultipleProblems(problems: List[Problem], msg: Text, ex: Option[Throwable]) extends Problem(msg, ex)