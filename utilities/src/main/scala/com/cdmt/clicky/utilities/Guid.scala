/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.utilities

/**
  *
  * Created by Unger
  *
  **/


import java.util.UUID

import com.cdmt.clicky.utilities.z._

import scala.reflect.ClassTag

class Guid private (val uuid: UUID) {
  lazy val uuidStr = uuid.toString.replace("-", "")
  override def toString = uuidStr
  override def hashCode: Int = uuid.hashCode()
  override def equals(thatObj: scala.Any): Boolean = thatObj match {
    case that: Guid => that.uuid == this.uuid
    case _ => false
  }
}

object Guid extends Logging {

  def create(): Guid = new Guid(UUID.randomUUID())

  val hashPattern = List(8, 12, 16, 20)

  def addDashes(str: String) = {
    val b = new StringBuilder()
    str.zipWithIndex.foreach {
      case (ch, ix) => {
        b.append(ch)
        if (hashPattern.contains(ix+1)) b.append('-')
      }
    }
    b.toString()
  }

  def option(str: String) = apply(str).toOption

  def apply(str: String): Problem Or Guid = {
    try {
      val strDashes = addDashes(str)
      val uuid = UUID.fromString(strDashes)
      val guid = new Guid(uuid)
      guid.succ
    } catch {
      case ex:Exception => {
        warn(s"Invalid value for Guid: $str", ex)
        ValidationProblem(str, "Guid").fail
      }
    }
  }

  val exampleString = "00000000000000000000000000000000"
  val example = new Guid(UUID.fromString(addDashes(exampleString)))

  //Build an example for some class that contains a Guid. Different example per class. Consistent by class.
//  def exampleT[T: ClassTag] = {
//    val ct = implicitly[ClassTag[T]]
//    val classInt = ct.runtimeClass.hashCode()
//    val fmt = classInt.formatted("%032d")
//    val dashed = addDashes(fmt)
//    new Guid(UUID.fromString(dashed))
//  }

  def exampleC[T](ct: ClassTag[T]) = {
//    val classInt = clazz.hashCode()
    val classInt = ct.runtimeClass.hashCode()
    val fmt = classInt.formatted("%032d")
    val dashed = addDashes(fmt)
    new Guid(UUID.fromString(dashed))
  }

  implicit val cbssd = CanBeStringSerDes.mkCBSSD[Guid](_.toString, str => Guid.option(str))
}

