/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.utilities

import com.cdmt.clicky.utilities.ValueClassesForUtilities._

import scala.collection._
import scala.concurrent.duration._

/**
  *
  * Created by Unger
  *
  **/


trait ScopedSetting[T] {
  def group: Name
  def name: Name
  def description: Text
}

trait ScopedSettingValue[T] {
  def setting: ScopedSetting[T]
  def scope: Option[ScopedKey[_]]
  def value: T

}

case class Switch(group: Name, name: Name, description: Text) extends ScopedSetting[Boolean]
case class Knob(group: Name, name: Name, description: Text) extends ScopedSetting[Double] //Goes past 11
case class Tag(group: Name, name: Name, description: Text) extends ScopedSetting[String]

case class SwitchValue(setting: Switch, scope: Option[ScopedKey[_]], value: Boolean) extends ScopedSettingValue[Boolean]
case class KnobValue(setting: Knob, scope: Option[ScopedKey[_]], value: Double) extends ScopedSettingValue[Double]
case class TagValue(setting: Tag, scope: Option[ScopedKey[_]], value: String) extends ScopedSettingValue[String]

object ScopedSettings {

  val registry = mutable.Buffer.empty[ScopedSetting[_]]

  private def register(setting: ScopedSetting[_]): Unit = {
    registry += setting
  }

  val writeEventsToDisk = register(Switch("FeatureToggle", "write-events-to-disk", "Should incoming events be written to disk"))
  val writeEventsToS3 = register(Switch("FeatureToggle", "write-events-to-S3", "Should incoming events be written to S3"))

}

class FeatureToggles() {
  //todo Quartz + EhCache to keep all values up to date
  def writeEventsToDisk = AutoHot().heat("FeatureToggle-GetValue-WriteEventsToDisk", 5.minutes){
    //todo: lookup setting
    true
  }

  def writeEventsToS3 = {
    true
  }

}

object FeatureToggles {
  val prod = new FeatureToggles()

  def apply() = prod
}