/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.utilities

/**
  *
  * Created by Unger
  *
  **/


import com.cdmt.clicky.utilities.z._

import scala.concurrent.Future

object FutureUtils extends Logging {
  import scala.concurrent.ExecutionContext.Implicits.global


  def FireFuture[T](f: => T) = Future {
    try {
      f.succ
    } catch {
      case t: Throwable => {
        warn("Exception in future!", t)
        ({}).fail
      }
    }
  }

}
