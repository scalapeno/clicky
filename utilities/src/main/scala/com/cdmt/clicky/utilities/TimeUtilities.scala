/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.utilities

/**
  *
  * Created by Unger
  *
  **/


import com.cdmt.clicky.utilities.Utils._
import com.cdmt.clicky.utilities.ValueClassesForUtilities._
import org.joda.time.{DateTime, DateTimeZone}

object TimeUtilities {

  val utcTimezone = DateTimeZone.UTC

  def utcYear(utcMillis: Millis): Year = {
    val dt = new DateTime(utcMillis.raw, utcTimezone)
    val yearInt = dt.year().get()
    Year(yearInt)
  }

  def utcDay(utcMillis: Millis): Day = {
    val dt = new DateTime(utcMillis.raw, utcTimezone)
    val dayInt = dt.dayOfYear().get()
    Day(dayInt)
  }

  def utcHour(utcMillis: Millis): Hour= {
    val dt = new DateTime(utcMillis.raw, utcTimezone)
    val hourInt = dt.hourOfDay().get()
    Hour(hourInt)
  }

  def dayForTimezone(utcMillis: Millis, timezone: DateTimeZone): Day = {
    val dt = new DateTime(utcMillis.raw, timezone)
    val dayInt = dt.dayOfYear().get()
    Day(dayInt)
  }

  def hourForTimezone(utcMillis: Millis, timezone: DateTimeZone): Hour = {
    val dt = new DateTime(utcMillis.raw, timezone)
    val hourInt = dt.hourOfDay().get()
    Hour(hourInt)
  }

  def tzForId(id: String) = tryToOption(DateTimeZone.forID(id))

}
