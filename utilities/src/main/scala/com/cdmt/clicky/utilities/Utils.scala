/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.utilities

/**
  *
  * Created by Unger
  *
  **/

import com.cdmt.clicky.utilities.ValueClassesForUtilities._
import com.cdmt.clicky.utilities.z._

object Utils {

  def tryToOption[T](f: => T) = {
    try {
      Some(f)
    } catch {
      case t: Throwable => None
    }
  }

  def tryToFailureWrapper[T](f: => T) = {
    try {
      f.succ
    } catch {
      case t: Throwable => MysteriousProblem("Unknown failure", Some(t))
    }
  }


}
