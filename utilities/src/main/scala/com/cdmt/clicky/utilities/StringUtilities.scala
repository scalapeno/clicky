/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.utilities

/**
 * Created by Unger
 */

object StringUtilities {

  val UTF8 = "UTF-8"

  implicit class StrUtils(val str: String) extends AnyVal {
    def tryToInt = {
      try {
        Option(str.toInt)
      } catch {
        case e: Exception => None
      }
    }

    def tryToLong = {
      try {
        Option(str.toLong)
      } catch {
        case e: Exception => None
      }
    }
  }

}
