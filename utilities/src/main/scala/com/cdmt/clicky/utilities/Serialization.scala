/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.utilities

import java.io.ByteArrayInputStream
import java.nio.ByteBuffer
import java.nio.charset.StandardCharsets

import com.cdmt.clicky.utilities.z._

/**
  *
  * Created by Unger
  *
  **/

class Serialization {

}

trait CanBeLine[T] {
  def toLine(t: T): Line
  def fromLine(line: Line): Problem Or T
}

object CanBeLine {
  def toLine[T: CanBeLine](t: T) = implicitly[CanBeLine[T]].toLine(t)
  def fromLine[T: CanBeLine](line: Line) = implicitly[CanBeLine[T]].fromLine(line)
}

trait CanBeStringSerDes[T] {
  def ser(t: T): String
  def des(str: String): Option[T]
}

object CanBeStringSerDes {
  def ser[T: CanBeStringSerDes](t:T): String = implicitly[CanBeStringSerDes[T]].ser(t)
  def des[T: CanBeStringSerDes](str: String): Option[T] = implicitly[CanBeStringSerDes[T]].des(str)

  def mkCBSSD[T](serialize: T => String, deserialize: String => Option[T]): CanBeStringSerDes[T] = new CanBeStringSerDes[T] {
    override def ser(t: T): String = serialize(t)
    override def des(str: String): Option[T] = deserialize(str)
  }
}

//A Line is a serialized record, with fields separated by '^', by convention
//Todo: don't allow ^ char in line
//todo: standardize writing to/from
class Line private(val str: String) {

  def withNewline: String = {
    if(str.contains('\n')) str else str + '\n'
  }

  def toByteBuffer: ByteBuffer = {
    val bytes = str.getBytes
    ByteBuffer.wrap(bytes)
  }

  def tokens: Array[String] = {
    str.split("\\^")
  }
}

object Line {
  def apply(str: String): Seq[Line] = str.split('\n').toSeq.map(s => new Line(s))
  def apply[T: CanBeLine](t: T): Line = implicitly[CanBeLine[T]].toLine(t)
  val empty = new Line("")
}

case class LineFile(lines: Seq[Line]) {
  def string = lines.map(_.str).mkString("", "\n", "")
  def toInputStream = new ByteArrayInputStream(string.getBytes(StandardCharsets.UTF_8))
}