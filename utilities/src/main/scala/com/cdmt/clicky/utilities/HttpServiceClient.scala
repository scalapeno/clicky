package com.cdmt.clicky.utilities

/**
  * Created by randy on 4/22/16.
  */

import com.google.api.client.http.GenericUrl
import com.google.api.client.http.javanet.NetHttpTransport


object HttpServiceClient {
  val prodInstance = new HttpServiceClient()
  def apply() = prodInstance
}

//todo: Use gRPC
class HttpServiceClient() extends Logging {

  def blockingRequest(url: String) = {

    val HTTP_TRANSPORT = new NetHttpTransport()
    val requestFactory = HTTP_TRANSPORT.createRequestFactory
    val gUrl = new GenericUrl(url)
    val request = requestFactory.buildGetRequest(gUrl)
    val res = request.execute()
    res
  }

}

