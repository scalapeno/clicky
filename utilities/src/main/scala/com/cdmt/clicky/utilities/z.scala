/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.utilities

import scalaz.Disjunction

/**
  *
  * Created by Unger
  *
  **/

object z {

  def fail[A, B](a: A) = Disjunction.left[A, B](a)
  def succ[A, B](b: B) = Disjunction.right[A, B](b)

  implicit def ToDisjunctionOps[A](a: A) = new DisjunctionOps(a)

  implicit class RichDisjunction[A, B](disj: Disjunction[A,B]) {
    def isFail = disj.isLeft
    def isSucc = disj.isRight
  }

  type Fail[A] = scalaz.-\/[A]
  val Fail = scalaz.-\/
  type Succ[A] = scalaz.\/-[A]
  val Succ = scalaz.\/-

  type Or[+A, +B] = scalaz.\/[A, B]
  val Or = scalaz.\/

  type NonEmptyList[A] = scalaz.NonEmptyList[A]
  val NonEmptyList = scalaz.NonEmptyList

}

class DisjunctionOps[A](val self: A) extends AnyVal {

  def fail[U] = Disjunction.left[A, U](self)
  def succ[U] = Disjunction.right[U, A](self)

//  def |@| = ???

}
