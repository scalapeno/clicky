/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.utilities

/**
  *
  * Created by Unger
  *
  **/


import com.cdmt.clicky.utilities.ValueClassesForUtilities._
import com.cdmt.clicky.utilities.z._
import org.apache.commons.validator.routines.UrlValidator
import play.api.libs.json._

trait ValidatedString[T] {
  protected def raw: String
  override def toString = raw
  override def equals(that: Any) = {
    that.toString == this.toString  //todo: technically not sound in case different class toString is the same
  }
  override def hashCode = raw.hashCode
}

trait ValidatedStringCompanion[T] {

  def validate(str: String): Problem Or T

  def apply(str: String): Problem Or T = {
    validate(str)
  }

  def option(str: String) = apply(str).toOption

  implicit val tCanBeStringSerDes = new CanBeStringSerDes[T] {
    override def ser(t: T): String = t.toString
    override def des(str: String): Option[T] = option(str)
  }

  def example(): T
}

class Url private(urlString: String) {
  override def toString = urlString
  override def equals(that: Any) = that match {
    case thatIp: Url => this.toString == thatIp.toString
    case _ => false
  }
  override def hashCode = urlString.hashCode
}

object Url {

  val urlValidator = new UrlValidator(UrlValidator.ALLOW_LOCAL_URLS)

  def apply(str: String): Problem Or Url = {
    if(urlValidator.isValid(str)) new Url(str).succ
    else ValidationProblem(str, "Url").fail
  }

  def option(str: String): Option[Url] = apply(str).toOption

  implicit val ipCanBeStringSerDes = new CanBeStringSerDes[Url] {
    override def ser(t: Url): String = t.toString
    override def des(str: String): Option[Url] = Url.option(str)
  }

  implicit val urlReads: Reads[Url] = Reads(jv => Url(jv.toString) match {
    case Fail(f) => JsError("couldn't json de serialize url")
    case Succ(u) => JsSuccess(u)
  })
  implicit val urlWrites = Writes((u: Url) => JsString(u.toString))
  implicit val urlFmt = Format(urlReads, urlWrites)

  val example = new Url("http://www.huffingtonpost.com/2013/04/30/wine-club-lot18_n_3165408.html")
}
