package com.cdmt.clicky.utilities

/**
  * Created by randy on 4/7/16.
  */


case class Template(str: String) {
  def render(arg: String) = Template.render(this, arg)
}

case class RenderedTemplate(str: String)

object Template {

  val separator = "@"

  def render(template: Template, arg: String): Option[RenderedTemplate] = {
    val upd = template.str.replaceAll("@@", arg)
    Some(RenderedTemplate(upd))
  }

  //not fast
  def render(template: Template, args: String*): Option[RenderedTemplate] = {
    var buff = template.str
    args.zipWithIndex.foreach { case(arg, ix) =>
      val indicator = separator + ix
      buff = buff.replaceAll(indicator, arg)
    }
    Some(RenderedTemplate(buff))
  }

}