/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.utilities

/**
  *
  * Created by Unger
  *
  **/

import com.cdmt.clicky.utilities.ValueClassesForUtilities._

trait ScopedKey[T] {
  def key: T
  def scope: ScopedKey.Type
}

object ScopedKey {
  trait Type {
    def label: Label
    def index: Index
  }

  object Type {
    def mkType(ix: Int, lab: String) = new Type {
      val label = lab.asLabel
      val index = ix.asIndex
    }

    val Global = mkType(1, "GlobalScope")
    val Partner = mkType(2, "PartnerScope") //The definition of keys that rely on Domain objects means this code should move higher on the stack.
  }

  val GlobalKey = new ScopedKey[Any] {
    val key = ScopedKey.Type.Global
    val scope = ScopedKey.Type.Global
  }


}