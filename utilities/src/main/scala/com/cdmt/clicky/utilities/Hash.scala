/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.utilities

/**
  *
  * Created by Unger
  *
  **/


import com.cdmt.clicky.utilities.ValueClassesForUtilities._
import net.openhft.hashing.LongHashFunction

object Hash {
  val prodInstance = new Hash()
  def apply() = prodInstance
}

class Hash {

  def string(s: String): HashId = {
    val l = LongHashFunction.farmUo().hashChars(s)
    HashId(l)
  }

  def string(s: String*): HashId = {
    val fullString = s.mkString("")
    val l = LongHashFunction.farmNa().hashChars(fullString)
    HashId(l)
  }

  def withinNamespace(id: HashId, s: String*): HashId = {
    val fullString = id.toString + s.mkString("")
    val l = LongHashFunction.farmNa().hashChars(fullString)
    HashId(l)
  }

  def withinNamespace(id: Guid, s: String*): HashId = {
    val fullString = id.toString + s.mkString("")
    val l = LongHashFunction.farmNa().hashChars(fullString)
    HashId(l)
  }

}

