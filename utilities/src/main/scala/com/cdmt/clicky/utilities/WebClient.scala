/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

//package com.cdmt.clicky.utilities
//
///**
//  *
//  * Created by Unger
//  *
//  **/
//
//import play.api.libs.ws._
//import play.api.libs.ws.ning.NingWSClient
//import scala.concurrent.Await
//import scala.concurrent.duration._
//import com.cdmt.clicky.utilities.ValueClassesForUtilities._
//
//class WebClient {
//  import scala.concurrent.ExecutionContext.Implicits.global
//
//  def blockOnUrl(url: Url, timeout: Duration = 10.seconds) = {
//    val wsClient = NingWSClient()
//    try {
//      val future = wsClient.url(url.raw).get()
//      val wsResponse = Await.result(future, timeout)
//      wsResponse
//    } finally {
//        wsClient.close()
//    }
//  }
//
//  def withUrl(url: Url)(action: WSResponse => Unit) = {
//    val wsClient = NingWSClient()
//    try {
//      val future = wsClient.url(url.raw).get()
//      future.onComplete(t => t.map(r => action(r)))
//    } catch {
//      case e: Exception => {
//        wsClient.close()
//      }
//    }
//  }
//
//  def withBodyFromUrl(url: Url)(action: String => Unit) = {
//    val wsClient = NingWSClient()
//    try {
//      val future = wsClient.url(url.raw).get()
//      future.onComplete(t => t.map(r => action(r.body)))
//    } catch {
//      case e: Exception => {
//        wsClient.close()
//      }
//    }
//  }
//
//}
