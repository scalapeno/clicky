/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.utilities

import org.slf4j.LoggerFactory

/**
 * Created by Unger
 */

trait Logging {

  private val logger = LoggerFactory.getLogger(this.getClass)

  protected def info(msg: String) = {
    logger.info(msg)
  }

  protected def warn(msg: String) = {
    logger.warn(msg)
  }

  protected def warn(msg: String, t: Throwable) = {
    logger.warn(msg, t)
  }

  protected def error(msg: String) = {
    logger.error(msg)
  }

  protected def error(msg: String, t: Throwable) = {
    logger.error(msg, t)
  }

}