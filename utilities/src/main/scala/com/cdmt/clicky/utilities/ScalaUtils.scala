/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.utilities

/**
  *
  * Created by Unger
  *
  **/

object ScalaUtils {

  def tryOrNone[T](f: => T) = {
    try {
      val res = f
      Option(res)
    } catch {
      case t: Throwable => None
    }
  }

}
