/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.utilities

/**
  *
  * Created by Unger
  *
  **/


import com.cdmt.clicky.utilities.ValueClassesForUtilities._
import org.joda.time.DateTime

object TimeProvider {

  def defaultNow = Millis(System.currentTimeMillis)

  private val prodInstance = new TimeProvider(defaultNow)

  def apply() = prodInstance
}

class TimeProvider(nowProvider: => Millis) {

  def now = nowProvider

  def newDateTime = new DateTime(now)

}
