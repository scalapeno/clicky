/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky.utilities

/**
  * Created by Unger
  */

import com.cdmt.clicky.utilities.StringUtilities._
import com.cdmt.clicky.utilities.z._
import play.api.libs.json._

object ValueClassesForUtilities extends TypeClassesForValueClassesForUtilities {

  //Strings
  case class Label(raw: String) extends AnyVal

  case class Name(raw: String) extends AnyVal

  case class Text(raw: String) extends AnyVal

  case class StringId(raw: String) extends AnyVal

//  case class Url(raw: String) extends AnyVal

  case class Title(raw: String) extends AnyVal

  //Ints
  case class Index(raw: Int) extends AnyVal

  case class Year(raw: Int) extends AnyVal

  case class Month(raw: Int) extends AnyVal

  case class Day(raw: Int) extends AnyVal

  case class Hour(raw: Int) extends AnyVal

  //Long
  case class HashId(raw: Long) extends AnyVal
  object HashId {
    def apply(s: String): Problem Or HashId = s.tryToLong match {
      case None => Fail(ValidationProblem(s, "HashId"))
      case Some(x) => Succ(HashId(x))
    }
  }

  case class Millis(raw: Long) extends AnyVal

  case class MicroDollars(raw: Long) extends AnyVal

  implicit class StringToDomainValueClasses(val underlying: String) extends AnyVal {
    def asName = Name(underlying)
    def asLabel = Label(underlying)
  }

  implicit class IntToDomainValueClasses(val underlying: Int) extends AnyVal {
    def asIndex = Index(underlying)
  }

  //Are these valuable? Why not String=>Name? Why not go both directions?
  //Perhaps this is less risky in a ground-up ValueClass project
  implicit def NameToString(n:Name): String = n.raw
  implicit def TitleToString(l: Title):String = l.raw
  implicit def LabelToString(l:Label):String = l.raw

  //Let's try both.
  implicit def StringToName(s: String): Name = Name(s)
  implicit def StringToTitle(s: String): Title = Title(s)
  implicit def StringToLabel(s: String): Label = Label(s)
  implicit def StringToText(s: String): Text = Text(s)

  //Long conversions
  implicit def HashIdToLong(h: HashId): Long = h.raw
  implicit def MillisToLong(m: Millis): Long = m.raw

  implicit def LongToMicroDollars(l: Long): MicroDollars = MicroDollars(l)

  implicit def LongToMillis(l: Long): Millis = Millis(l)

}

trait TypeClassesForValueClassesForUtilities {
  self: ValueClassesForUtilities.type =>

  implicit val millisCBSD = CanBeStringSerDes.mkCBSSD[Millis](_.raw.toString, str => {
    str.tryToLong.map(l => Millis(l))
  })

  implicit val hashIdCBSD = CanBeStringSerDes.mkCBSSD[HashId](_.raw.toString, str => {
    str.tryToInt.map(l => HashId(l))
  })


  implicit val titleFmt = Format(Reads((jv) => JsSuccess(Title(jv.toString))), Writes((t: Title) => JsString(t.raw)))

}
