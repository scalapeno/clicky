package com.cdmt.clicky

import javax.servlet.http.HttpServletRequest

import com.cdmt.clicky.domain.RuleSetId
import com.cdmt.clicky.domain.advertiser.UserId
import com.cdmt.clicky.services.TemplateService
import com.cdmt.clicky.utilities.ValueClassesForUtilities.Name
import com.cdmt.clicky.utilities.z._
import com.cdmt.clicky.utilities.{CanBeStringSerDes, HttpServiceClient, Logging, Problem}
import org.scalatra.{ApiFormats, BadRequest, InternalServerError}

/**
  * Created by randy on 3/30/16.
  */
class TemplateServlet extends ClickyStack with ApiFormats with Logging {
/*
Features:
Serve multiple impressions simultaneously (dedupable)
Fewest possible request/response roundtrips
Push to CDN?
Lowest data usage/fastest load time
 */



  //
  //
  //  abstract class Endpoint(
  //                         val route: Url
  //                         , val methods: List[HttpMethod]
  //                         )
  //
  //  abstract class JsEndpoint(
  //                           route: Url
  //                           , methods: List[HttpMethod]
  //                         ) extends Endpoint(route, methods)
  //
  //  abstract class JsonEndpoint(
  //                             route: Url
  //                             , methods: List[HttpMethod]
  //                           ) extends Endpoint(route, methods)
  //

  case class ParameterNotSuppliedProblem(name: Name) extends Problem(s"Parameter is required: $name", None)

  class InputParameter[A](val name: Name, val source: Name => Option[String], val constructor: String => Problem Or A)
  case class QueryParameter[A](n: Name, c: String => Problem Or A) extends InputParameter(n, _ => params.get(n), c)

  val rsidParam = QueryParameter("rsid", RuleSetId.apply)

  def validate[A](param1: InputParameter[A])(implicit request: HttpServletRequest): NonEmptyList[_ <:Problem] Or A = {
    param1.source(param1.name) match {
      case Some(param) => param1.constructor(param) match {
        case Fail(f) => NonEmptyList(f).fail
        case Succ(a) => a.succ
      }
      case None => {
        NonEmptyList(ParameterNotSuppliedProblem(param1.name.raw)).fail
      }
    }
  }

  //Loader endpoint. An extra layer of indirection. Gets the javascript that will call for the widget iframe.
  get("/l/template.js") {

    info("request for template.js")

    val userId = cookies.get("userId") match {
      case None => writeUserCookie(request)
      case Some(uId) => UserId(uId).getOrElse(writeUserCookie(request))
    }

    //Extract/validate all input params so we can prevent questionable values
    //prevent complex input types from invading underlying layers
    //rsid

//    val x = params.get("rsid") -> RuleSetId.apply _

//    validate(params.get("rsid") -> RuleSetId.apply)


    //Hand off to App

    //Serialize response

    val ruleSetIdO = params.get("rsid")
    info(s"request for template.js, rsid: $ruleSetIdO")

    val rsidO = for {
      ruleSetIdStr <- ruleSetIdO
      ruleSetId <- CanBeStringSerDes.des[RuleSetId](ruleSetIdStr)
    } yield ruleSetId

    //todo: could this be cleaner with the "validate" stuff above?
    rsidO match {
      case None => BadRequest()
      case Some(rsid) => {
        val tmpl = TemplateService().templateForRuleSetId(rsid)
        tmpl match {
          case Fail(p) => BadRequest()
          case Succ(template) => {
            val rendered = template.render(rsid.toString)
            rendered match {
              case None => InternalServerError()
              case Some(rt) => {
                contentType = formats("js")
                val out = rt.str
//                info(s"returning template: ${out}")
                out
              }
            }
          }
        }
      }
    }

  }

  get("/t/:id") {
    val templateId = params("id")
    info(s"request for iframe template: $templateId")

    val userId = cookies.get("userId") match {
      case None => {
        writeUserCookie(request)
      }
      case Some(uId) => UserId(uId).getOrElse(writeUserCookie(request))
    }

    info(s"userid: ${userId.id}")
    val id = userId.id

    //todo: This path is a dependency. Should be expressed as such.
    val dataResp = HttpServiceClient().blockingRequest("http://localhost:8080/adCall")
    //blocking call - another candidate for RX

    val data = dataResp.parseAsString()

//    val handleBars = new Handlebars()
//    val src = new URLTemplateSource()
//    val templatePath = this.getClass.getClassLoader.getResource(".").toString + "../../src/main/webapp", "/"
//    val template = handleBars.compile(templatePath)

    getTemplate(templateId, id, data)
  }

  //Todo: Make this more customizable
  //todo: wtf do i have my own templates for. use something pre-existing.
  //todo NEXT: Get something widget-like rendered from a template
  def getTemplate(templateId: String, id: String, data: String) = {

    val base = <html>
      <body>
        <h1>Template {templateId}</h1>
        <h1>User {id}</h1>
        <div id="goHere"></div>
        <h1>Foofer</h1>
        <div>
          <span><a href=".."><span class="thumb" style="background-image: url(&quot;http://images.taboola.com/taboola/image/fetch/dpr_2.0%2Cf_jpg%2Cq_80%2Ch_135%2Cw_202%2Cc_fill%2Cg_faces%2Ce_sharpen/http%3A//ugc-01.cafemomstatic.com/gen/constrain/560/380/80/2016/01/28/15/5j/ul/ph2sg8ykws1.jpg&quot;);"></span></a><a href="../.."><span>Article Title</span></a></span>
          <a href=".."><span class="thumb" style="background-image: url(&quot;http://images.taboola.com/taboola/image/fetch/dpr_2.0%2Cf_jpg%2Cq_80%2Ch_135%2Cw_202%2Cc_fill%2Cg_faces%2Ce_sharpen/http%3A//cdn.taboolasyndication.com/libtrc/static/thumbnails/722d6e9d8c25e70a373246d0c9f4fa2f.png&quot;);"></span></a><a href="../.."><span>Article Title2</span></a>
        </div>
        <div data-item-id="~~V1~~1884019816403375594~~_DrDR0tvo3_Xoz8533jAzuLo7ZtYUPCLyV3Rou3MwmQC5xsnLMQkNbbO3QBcrJMnKVsZ4cM6ZySo9FX1U3zSPQ" data-item-title="17 Old-School Photos of Kim Kardashian That Will Make You Do a Double Take" data-item-thumb="http://ugc-01.cafemomstatic.com/gen/constrain/560/380/80/2016/01/28/15/5j/ul/ph2sg8ykws1.jpg" data-item-syndicated="true" class="videoCube trc_spotlight_item origin-default thumbnail_top syndicatedItem photoItem videoCube_2_child trc_excludable "><a title="17 Old-School Photos of Kim Kardashian That Will Make You Do a Double Take" href="http://trc.taboola.com/aol-huffingtonpost/log/3/click?pi=%2Fentry%2Ftrump-campaign-payments_us_5768a69ee4b0853f8bf1fe2d&amp;ri=dbe6a1601417b04fe1a1d8174e2f2ed6&amp;sd=v2_034a71eaf42ee4cbbd92121e6dcf7eac_3c83218f-ef7c-42e6-82e5-5356a1ee1bca_1466525354_1466525354_CIi3jgYQisE_GJyf5J3XKiAB&amp;ui=3c83218f-ef7c-42e6-82e5-5356a1ee1bca&amp;it=photo&amp;ii=~~V1~~1884019816403375594~~_DrDR0tvo3_Xoz8533jAzuLo7ZtYUPCLyV3Rou3MwmQC5xsnLMQkNbbO3QBcrJMnKVsZ4cM6ZySo9FX1U3zSPQ&amp;pt=text&amp;li=rbox-t2m&amp;redir=http%3A%2F%2Fthestir.cafemom.com%2Fcelebrities%2F195391%2F17_oldschool_photos_of_kim%3Futm_medium%3Dsem2%26utm_source%3Dtaboola-pvs%26utm_campaign%3Dtaboola_pvs%26utm_content%3Daol-huffingtonpost&amp;vi=1466525355932&amp;p=cafemom-2013-sc&amp;r=99&amp;ppb=CNoH&amp;cpb=EhYyMTQtUkVMRUFTRS0ke3ZlcnNpb259GIANIAcqGWxhLnRhYm9vbGFzeW5kaWNhdGlvbi5jb20yCHdhdGVyMjE1" rel="nofollow" target="_blank" class=" item-thumbnail-href "><div class="thumbBlock_holder"><span class="thumbBlock" style="background-image: url(&quot;http://images.taboola.com/taboola/image/fetch/dpr_2.0%2Cf_jpg%2Cq_80%2Ch_135%2Cw_202%2Cc_fill%2Cg_faces%2Ce_sharpen/http%3A//ugc-01.cafemomstatic.com/gen/constrain/560/380/80/2016/01/28/15/5j/ul/ph2sg8ykws1.jpg&quot;);"><span class="thumbnail-overlay"></span><span class="branding">The Stir</span><span class="static-text top-right"></span></span><div class="videoCube_aspect"></div></div></a><a title="17 Old-School Photos of Kim Kardashian That Will Make You Do a Double Take" href="http://thestir.cafemom.com/celebrities/195391/17_oldschool_photos_of_kim?utm_medium=sem2&amp;utm_source=taboola-pvs&amp;utm_campaign=taboola_pvs&amp;utm_content=aol-huffingtonpost" rel="nofollow" target="_blank" class=" item-label-href "><span class="video-label-box"><span class="video-label video-title trc_ellipsis " style="-webkit-line-clamp: 3;">17 Old-School Photos of Kim Kardashian That Will Make You Do a Double Take</span><span class="branding">The Stir</span></span></a><div class=" trc_user_exclude_btn " title="Remove this item"></div><div class=" trc_exclude_overlay trc_fade "></div><div class=" trc_undo_btn ">Undo</div></div>
      </body>

      <script id="entry-template" type="text/x-handlebars-template" src="/html/blogPost.hbs"></script>
      <script src="https://cdn.jsdelivr.net/handlebarsjs/4.0.5/handlebars.min.js"></script>
      <script src="/js/widget.js"></script>
      <link href="/css/widget.css" rel="stylesheet" type="text/css"></link>
    </html>

    base.toString().replace("@@", data)
  }

  def writeUserCookie(implicit request: HttpServletRequest): UserId = {
    val userId = UserId.create()
    cookies.set("userId", userId.id)
    userId
  }

}
