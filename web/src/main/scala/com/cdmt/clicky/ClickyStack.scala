/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky

import com.cdmt.clicky.utilities.Logging
import org.scalatra._
import org.scalatra.scalate.ScalateSupport

trait ClickyStack extends ScalatraServlet with ScalateSupport with Logging {

  notFound {
    // remove content type in case it was set through an action
    contentType = null
    // Try to render a ScalateTemplate if no route matched
    findTemplate(requestPath) map { path =>
      contentType = "text/html"
      layoutTemplate(path)
    } orElse serveStaticResource() getOrElse resourceNotFound()
  }

  error(x)

  val x: org.scalatra.ErrorHandler = {
    case t: Throwable => error(s"Uncaught exception", t)
  }

}
