/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky

import org.eclipse.jetty.server.Server
import org.eclipse.jetty.webapp.WebAppContext

/**
 * Created by Unger
 */

object WebServer extends App {
  val port = 8080
  val server = new Server(port)
  val classPath = this.getClass.getClassLoader.getResource(".").toString
  val context = new WebAppContext(classPath + "../../src/main/webapp", "/")
  context.setServer(server)
  server.setHandler(context)
  server.start()
}
