/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

package com.cdmt.clicky

import com.cdmt.clicky.adserver.{AdServer, NativeAdPackage, RedirectAd}
import com.cdmt.clicky.clicklogger.ClickLogger
import com.cdmt.clicky.clickredirect.ClickRedirect
import com.cdmt.clicky.domain.ValueClassesForDomain._
import com.cdmt.clicky.domain._
import com.cdmt.clicky.domain.advertiser._
import com.cdmt.clicky.domain.events.{ClickEvent, OpportunityEvent}
import com.cdmt.clicky.domain.publisher.OpportunityCoordinates
import com.cdmt.clicky.services._
import com.cdmt.clicky.utilities.ValueClassesForUtilities.Title
import com.cdmt.clicky.utilities._
import com.cdmt.clicky.utilities.z._
import org.joda.time.DateTime
import org.scalatra.ApiFormats
import play.api.libs.json.Json

object ClickyServlet {
  val instance = new ClickyServlet
  val adRequestPath = "/adCall"
}

class ClickyServlet extends ClickyStack with ApiFormats with Logging {

  get("/") {
    <html>
      <body>
        <h1>Hello, world!</h1>
        Say
        <a href="hello-scalate">hello to Scalate</a>
        .
      </body>
    </html>
  }

  get("/exc") {
    ???
  }

  case class SimpleAPIFormat(title: Title, img: Url, displayUrl: Url, clickUrl: Url)

  object SimpleAPIFormat {
    def apply(redirAd: RedirectAd): SimpleAPIFormat = {
      redirAd.ad match {
        case n: NativeAdPackage => SimpleAPIFormat(n.title, n.image, n.displayUrl, redirAd.clickUrl)
        case _ => {
          error("Unknown ad type")
          throw new IllegalStateException()
        }
      }
    }
    implicit val jfmt = Json.format[SimpleAPIFormat]
  }

  //JS/iframe implementation
  get(ClickyServlet.adRequestPath) {
    contentType = formats("json")
    //Extract params
    //Todo: use string rep
    val partner = PartnerId.example.id
    val ruleSetId = PubRuleSetId.example.id
    val url = Url.example.toString
    val ppid = PPID.example.toString
    val user = UserId.example.id
    val ip = IpAddress.example.toString
    val ua = UserAgentHeader("iphone").raw

    //Request metadata
    val serverTime = TimeProvider().now

    //Package up these params
    val oppOr = OpportunityCoordinates(partner, ruleSetId, url, ppid, user, ip, ua)

    //Invoke Service to assemble package to serve
    //Todo: move this to service
    val response = oppOr match {
      case Succ(opp) => {
        val adO = AdServer().opportunityFor(opp)
        adO match {
          case Fail(f) => {
            warn("Could not generate an ad")
            f
          }
          case Succ(ad) => {
            val fmt = SimpleAPIFormat(ad)
            val jv = Json.toJson(fmt)
            val jStr = Json.asciiStringify(jv)
            jStr
          }
        }
      }
      case Fail(f) => {
        //todo: Log failure to return an ad!
        warn("Could not identify the opportunity")
        f
      }
    }

    response
  }

  //API
  get("/serve") {
    //Extract params
    //Invoke Service to assemble package to serve
    //Serialize
  }

  val defaultParamValue = "NULL"

  get("/cl") {
    //todo: push unzipping, etc down to a Application level. Or application that ties zipping, persisting, redirecting?
    val millis = TimeProvider().now
    val userId = (cookies.get("userId") orElse params.get("userId")).getOrElse("")
    val payload = params.get(ClickRedirect.clickParamName).getOrElse(defaultParamValue)
    val ua = Option(request.getHeader("User-Agent")).getOrElse(defaultParamValue)
    val ip = Option(request.getRemoteAddr).getOrElse(defaultParamValue)

    //Send Params
    ClickLogger().logClick(millis, userId, payload, ua, ip) match {
      case Succ(url) => {
        halt(status = 301, headers = Map("Location" -> url.toString))
      }
      case Fail(nel) => halt(status = 500, body = nel.toString)
    }
  }

  get("/fakeClick0") {
    val rcvd = new DateTime()
    val impId = ImpressionId("123").toString
    val oppId = OppId("12345").toString
    val creativeId = CreativeId("789").toString
    val userId = Guid.create().toString
    val partnerId = PartnerId("1234").toString
    val ppid = PPID("").toString
    val ua = Option(request.getHeader("User-Agent")).map(u => UserAgentHeader(u)).toString
    val ipAddressO = IpAddress(request.getRemoteHost)
    ipAddressO match {
      case Fail(w) => error("No Ip")
      case Succ(ipAddress) => {
        info(s"Received request for fake click 1")
        val clickV = ClickEvent(impId, oppId, partnerId, ppid, rcvd.getMillis, ipAddress.toString, ua, userId)
        val click = clickV.getOrElse(???)
        EventPersistence.clickPersistence.persist(click)
        Line(click).withNewline
      }
    }
  }

  get("/meta") {

  }

  get("/fakeBid") {
    val oppId = OppId("12345").toString //todo: Hash components for ID
    val partnerId = PartnerId.example.toString
    val userId = Guid.create().toString
    val ua = UserAgentHeader(request.getHeader("User-Agent")).toString
    val ipAddressO = IpAddress(request.getRemoteHost)
    val ppid = PPID("").toString
    val ruleSetId = RuleSetId("").toString
    ipAddressO match {
      case Fail(_) => {
        error("No Ip")
      }
      case Succ(ipAddress) => { //Todo: All of this logic should live in a service layer below API
        info(s"Received request for fake bid 1")
        val oppV = OpportunityEvent(oppId, partnerId, Url.example.toString, userId, ipAddress.toString, ua, ppid, ruleSetId)
        val opp = oppV.getOrElse(throw new IllegalStateException("blah"))
        EventPersistence.oppPersistence.persist(opp)
        Line(opp).withNewline
      }
    }
  }

}