
/*
 * Copyright (c) 2016 - Randy Unger. All Rights Reserved.
 */

import javax.servlet.ServletContext

import com.cdmt.clicky.{AssetServlet, ClickyServlet, TemplateServlet}
import org.scalatra._

class ScalatraBootstrap extends LifeCycle {

  override def init(context: ServletContext) {
//    context.mount(ClickyServlet.instance, "/*")
    context.mount(new ClickyServlet, "/*")
    context.mount(new TemplateServlet, "/t/*")
    context.mount(new AssetServlet, "/a/*")
  }
}

